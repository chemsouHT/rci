# from django
from datetime import datetime

from django.contrib.auth import get_user_model
from django.db.models import Q
from django.shortcuts import get_object_or_404

from rest_framework import generics, permissions
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from .pagination import PostDisplayPagination
from .serializers import PostModelSerializer, PostModelCreateSerializer
from hashtag.models import Hashtag
from post.models import Post
from comment.models import Comment


UserModel = get_user_model()

#the api view
class PostListAPIView(generics.ListAPIView):
    serializer_class    = PostModelSerializer           # the serializer class we made in .serializers
    permission_classes  = [permissions.IsAuthenticated] #user must be authenticated
    pagination_class    = PostDisplayPagination

    def get_queryset(self, *args, **kwargs):#override a method
        post_queryset    = Post.objects.all()#all posts
        queryContent     = self.request.GET.get("q",None)# get the query
        try:
            request_user_groups = self.request.user.profile.get_groups() #we get the request user
            post_queryset = post_queryset.filter(group__in=request_user_groups)#we filter posts which's group is in user's groups
        except:
            print('ERROR : Cannot get posts from groups where request_user is member in')

        if queryContent is not None:
            post_queryset    = post_queryset.filter(   #we filter as the query
                                Q(content__icontains=queryContent)|
                                Q(user__username=queryContent)|
                                Q(group__name=queryContent)
                                )
        return post_queryset.order_by('-timestamp')   #data

class PostGroupListAPIView(generics.ListAPIView):
    serializer_class    = PostModelSerializer           # the serializer class we made in .serializers
    permission_classes  = [permissions.IsAuthenticated] #user must be authenticated
    pagination_class    = PostDisplayPagination

    def get_queryset(self, *args, **kwargs):
        group_name = self.kwargs.get('name')
        post_queryset = Post.objects.filter(group__name=group_name).order_by('-timestamp')

        return post_queryset

class PostHashtagListAPIView(generics.ListAPIView):
    serializer_class    = PostModelSerializer           # the serializer class we made in .serializers
    permission_classes  = [permissions.IsAuthenticated] #user must be authenticated
    pagination_class    = PostDisplayPagination

    def get_queryset(self, *args, **kwargs):
        tag_name = self.kwargs.get('tag')
        hashtag    = get_object_or_404(Hashtag, tag=tag_name)
        post_queryset = Post.objects.filter(pk__in=hashtag.get_posts()).order_by('-timestamp')
        try:
            request_user_groups = self.request.user.profile.get_groups() #we get the request user
            post_queryset = post_queryset.filter(group__in=request_user_groups)#we filter posts which's group is in user's groups
        except:
            print('ERROR : Cannot get posts from groups where request_user is member in')

        return post_queryset

class PostUserListAPIView(generics.ListAPIView):
    serializer_class    = PostModelSerializer           # the serializer class we made in .serializers
    permission_classes  = [permissions.IsAuthenticated] #user must be authenticated
    pagination_class    = PostDisplayPagination

    def get_queryset(self, *args, **kwargs):
        username                    = self.kwargs.get('username')
        userposts_queryset          = Post.objects.filter(user__username=username)
        usercomments_posts_queryset = Comment.objects.get_posts_by_user(username)
        post_queryset = (userposts_queryset | usercomments_posts_queryset).order_by("-timestamp")
        try:
            user = get_object_or_404(UserModel, username=username)
            user_groups = user.profile.get_groups() #we get the request user
            post_queryset = post_queryset.filter(group__in=user_groups)#we filter posts which's group is in user's groups
            print(user_groups)
        except:
            print('ERROR : Cannot get posts from groups where request_user is member in')
        return post_queryset

class PostCreateAPIView(generics.CreateAPIView):
    serializer_class    = PostModelCreateSerializer     # the serializer class we made in .serializers
    permission_classes  = [permissions.IsAuthenticated] #user must be authenticated
    def perform_create(self, serializer):               # create the post
        print(self)
        serializer.save(user = self.request.user)

class PostUpdateAPIView(generics.UpdateAPIView):
    serializer_class    = PostModelSerializer
    permission_classes  = [permissions.IsAuthenticated]
    queryset            = Post.objects.all()

    def perform_update(self, serializer):
        serializer.save(last_update_time = datetime.now())

class PostDetailAPIView(generics.ListAPIView):
    serializer_class    = PostModelSerializer
    permission_classes  = [permissions.IsAuthenticated]
    pagination_class    = PostDisplayPagination


    def get_object(self, *args, **kwargs):
        post_id     = self.kwargs.get('pk')
        post        = get_object_or_404(Post, pk = post_id)
        return post

    def get_queryset(self, *args, **kwargs):
        post_id     = self.kwargs.get('pk')
        post        = Post.objects.filter(pk=post_id)
        if post :
             return post.first().get_replies()
        return None

class RePostAPIView(APIView):
    permission_classes  = [permissions.IsAuthenticated]
    def get(self, request, pk, format = None):
        group__id = request.GET.get('group')
        post_queryset = Post.objects.filter(pk = pk)
        if post_queryset.exists() and post_queryset.count() == 1 :
            post_to_repost = post_queryset.first()
            if request.user.is_authenticated:
                repost = Post.objects.repost(request.user, post_to_repost, group__id)
                if repost is not None:
                    data = PostModelSerializer(repost).data
                    return Response(data)
                return Response({"message" : "Vous ne pouvez pas partager ce contenu"}, status = 400 )
            return Response({"message" : "Vous devez vous connecter avant"}, status = 400 )
        return Response({"message" : "Pas de publication à partager"}, status = 400 )

class RePlyAPIView(APIView):
    permission_classes  = [permissions.IsAuthenticated]
    def get(self, request, pk, format = None):
        reply_content = request.GET.get('content')
        post_queryset = Post.objects.filter(pk = pk)
        if post_queryset.exists() and post_queryset.count() == 1 :
            post_to_reply_to = post_queryset.first()
            if request.user.is_authenticated:
                reply = Post.objects.reply(request.user, post_to_reply_to, reply_content)
                if reply is not None:
                    data = PostModelSerializer(reply).data
                    return Response(data)
                return Response({"message" : "Vous ne pouvez pas répondre à ce contenu"}, status = 400 )
            return Response({"message" : "Vous devez vous connecter avant"}, status = 400 )
        return Response({"message" : "Pas de publication pour répondre"}, status = 400 )

class PostUpToggleAPIView(APIView):
    permission_classes  = [permissions.IsAuthenticated]
    def get(self, request, pk, format = None):
        post_queryset = Post.objects.filter(pk = pk)
        if request.user.is_authenticated:
            is_upped = Post.objects.up_toggle(request.user, post_queryset.first())
            return Response({"upped" : is_upped})
        return Response({"message" : "Utilisateur Anonyme"}, status = 400 )
