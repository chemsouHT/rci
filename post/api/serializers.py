from django.urls import reverse_lazy
from django.utils.timesince import timesince

from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault

from account.api.serializers import UserModelSerializer
from group.api.serializers import GroupModelSerializer
from post.models import Post


#the serialized class of the parent of a post
class PostParentModelSerializer(serializers.ModelSerializer):
    last_update_time_display    = serializers.SerializerMethodField() #additional field 'last_update_time_display' we get it with get_last_update_time_display()
    date_display                = serializers.SerializerMethodField() #additional field 'date_display' we get it with get_date_display()
    age                         = serializers.SerializerMethodField() #additional field 'age' we get it with get_age()
    comments_url                = serializers.SerializerMethodField()
    info_url                    = serializers.SerializerMethodField()
    create_comment_url          = serializers.SerializerMethodField()
    user                        = UserModelSerializer(read_only=True)
    group                       = GroupModelSerializer(read_only=True)
    # parent                      = PostModelSerializer(read_only=True)
    class Meta:
        model  = Post   # the model to get fields from
        fields = [      # fields must be the same as the model fields/additional fields
        'id',
        'parent',
        'group',
        'user',
        'content',
        'timestamp',
        'is_repost',
        'is_reply',
        'age',
        'date_display',
        'last_update_time',
        'last_update_time_display',
        'comments_url',
        'info_url',
        'create_comment_url'
        ]


    def get_date_display(self, object):
        return object.timestamp.strftime("%b %d %I:%M %p ")#format timestamp

    def get_last_update_time_display(self, object):
        return object.last_update_time.strftime("%b %d %I:%M %p ")#format last_update_time

    def get_comments_url(self, object):
        return reverse_lazy("api-comment:post", kwargs={'pk':object.id})

    def get_age(self, object):
        return timesince(object.timestamp)

    def get_info_url(self, object):
        return reverse_lazy("post:detail", kwargs={'pk':object.id})

    def get_create_comment_url(self, object):
        return reverse_lazy("api-comment:create", kwargs={'pk':object.id})

#serialized class for Post model
class PostModelSerializer(serializers.ModelSerializer):
    last_update_time_display    = serializers.SerializerMethodField() #additional field 'last_update_time_display' we get it with get_last_update_time_display()
    date_display                = serializers.SerializerMethodField() #additional field 'date_display' we get it with get_date_display()
    age                         = serializers.SerializerMethodField() #additional field 'age' we get it with get_age()
    comments_url                = serializers.SerializerMethodField()
    info_url                    = serializers.SerializerMethodField()
    create_comment_url          = serializers.SerializerMethodField()
    ups                         = serializers.SerializerMethodField()
    up_url                      = serializers.SerializerMethodField()
    did_user_up                 = serializers.SerializerMethodField()
    img_url                     = serializers.SerializerMethodField()
    file_url                    = serializers.SerializerMethodField()
    user                        = UserModelSerializer(read_only=True)
    group                       = GroupModelSerializer(read_only=True)
    parent                      = PostParentModelSerializer(read_only=True)
    class Meta:
        model  = Post   # the model to get fields from
        fields = [      # fields must be the same as the model fields/additional fields
        'id',
        'parent',
        'group',
        'user',
        'content',
        'timestamp',
        'is_repost',
        'is_reply',
        'age',
        'ups',
        'photo',
        'img_url',
        'file',
        'file_url',
        'uppers',
        'did_user_up',
        'date_display',
        'last_update_time',
        'last_update_time_display',
        'comments_url',
        'info_url',
        'create_comment_url',
        'up_url',
        ]


    def get_date_display(self, object):
        return object.timestamp.strftime("%b %d %I:%M %p ")#format timestamp

    def get_last_update_time_display(self, object):
        return object.last_update_time.strftime("%b %d %I:%M %p ")#format last_update_time

    def get_comments_url(self, object):
        return reverse_lazy("api-comment:post", kwargs={'pk':object.id})

    def get_age(self, object):
        return timesince(object.timestamp)

    def get_info_url(self, object):
        return reverse_lazy("post:detail", kwargs={'pk':object.id})

    def get_create_comment_url(self, object):
        return reverse_lazy("api-comment:create", kwargs={'pk':object.id})

    def get_img_url(self, object):
        if object.photo:
            return object.photo.url
        return None

    def get_file_url(self, object):
        if object.file:
            return object.file.url
        return None

    def get_up_url(self, object):
        return reverse_lazy("api-post:up", kwargs={'pk':object.id})

    def get_ups(self, object):
        return object.uppers.all().count()

    def get_did_user_up(self, object):
        request = self.context.get("request")
        if(request):
            request_user = request.user
            if request_user.is_authenticated:
                return request_user in object.uppers.all()
        return False





class PostModelCreateSerializer(serializers.ModelSerializer):
    last_update_time_display    = serializers.SerializerMethodField() #additional field 'last_update_time_display' we get it with get_last_update_time_display()
    date_display                = serializers.SerializerMethodField() #additional field 'date_display' we get it with get_date_display()
    age                         = serializers.SerializerMethodField() #additional field 'age' we get it with get_age()
    comments_url                = serializers.SerializerMethodField()
    info_url                    = serializers.SerializerMethodField()
    user                        = UserModelSerializer(read_only=True)
    # group                       = serializers.ChoiceField(choices=['A','B'])
    class Meta:
        model  = Post   # the model to get fields from
        fields = [      # fields must be the same as the model fields/additional fields
        'id',
        'group',
        'user',
        'content',
        'photo',
        'file',
        'timestamp',
        'is_repost',
        'is_reply',
        'age',
        'date_display',
        'last_update_time',
        'last_update_time_display',
        'comments_url',
        'info_url'
        ]


    def get_date_display(self, object):
        return object.timestamp.strftime("%b %d %I:%M %p ")#format timestamp

    def get_last_update_time_display(self, object):
        return object.last_update_time.strftime("%b %d %I:%M %p ")#format last_update_time

    def get_comments_url(self, object):
        return reverse_lazy("api-comment:post", kwargs={'pk':object.id})

    def get_age(self, object):
        return timesince(object.timestamp)

    def get_info_url(self, object):
        return reverse_lazy("post:detail", kwargs={'pk':object.id})

    def get_group(self, object):
        print('hello')
        return object.user.profile.get_groups()
