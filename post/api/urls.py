
from django.urls import path

from .views import (
                PostDetailAPIView,
                PostListAPIView,
                PostCreateAPIView,
                PostGroupListAPIView,
                PostHashtagListAPIView,
                PostUpdateAPIView,
                PostUpToggleAPIView,
                PostUserListAPIView,
                RePlyAPIView,
                RePostAPIView
                )


app_name    = 'api-post'

urlpatterns = [
    path('list/', PostListAPIView.as_view(), name='list'),#/api/post/list
    path('create/', PostCreateAPIView.as_view(), name='create'),#/api/post/create
    path('<int:pk>/', PostDetailAPIView.as_view(), name='detail'),#/api/post/int/update
    path('<int:pk>/update/', PostUpdateAPIView.as_view(), name='update'),#/api/post/int/update
    path('<int:pk>/repost/', RePostAPIView.as_view(), name='repost'),#/api/post/int/repost
    path('<int:pk>/reply/', RePlyAPIView.as_view(), name='reply'),#/api/post/int/reply
    path('<int:pk>/up/', PostUpToggleAPIView.as_view(), name='up'),#/api/post/int/up
    path('<str:name>/group', PostGroupListAPIView.as_view(), name='group'),#/api/post/group_name/group
    path('<str:tag>/tag', PostHashtagListAPIView.as_view(), name='tag'),#/api/post/tag/tag
    path('<str:username>/user', PostUserListAPIView.as_view(), name='user'),#/api/post/username/user
]
