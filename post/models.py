from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.shortcuts import get_object_or_404
from django.urls import reverse

from group.models import Group
from hashtag.signals import parsed_hashtags


import re




#the manager of the post model
class PostManager(models.Manager):
    def repost(self, user, parent_post, group_id):
        group  = get_object_or_404(Group, pk = group_id)
        if (parent_post.parent):
            parent_post = parent_post.parent  #we set the original parent only
        repost = Post.objects.create(
                    user        = user,
                    content     = parent_post.content,
                    photo       = parent_post.photo,
                    file       = parent_post.file,
                    parent      = parent_post,
                    group       = group,
                    is_repost   = True
                    )
        repost.save()
        return repost

    def reply(self, user, parent_post, content):
        if parent_post.parent and parent_post.is_reply:
            parent_post = parent_post.parent  #we set the original parent only if it's a reply

        reply = None
        if content:
            reply = Post.objects.create(
                        user        = user,
                        content     = content,
                        parent      = parent_post,
                        group       = parent_post.group,
                        is_reply   = True
                        )
            reply.save()
        return reply

    def up_toggle(self, user, post):
        upped_by_user = user in post.uppers.all()

        if upped_by_user:#if it's already upped so we will unlike
            post.uppers.remove(user)
            return False # we unlike
        else :
            post.uppers.add(user)
            return True # we like

#the model of Posts in the database
class Post(models.Model):
    user        = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE)#the owner when we delete him, all his posts are deleted
    group       = models.ForeignKey(Group, default=1, on_delete=models.CASCADE)#the attached group when we delete it, all his posts are deleted
    parent      = models.ForeignKey('self',on_delete=models.SET_NULL, blank=True, null=True)#the parent of the post if it's a reply or response
    uppers      = models.ManyToManyField(settings.AUTH_USER_MODEL,related_name='upped', blank=True)#users who upped this post

    content     = models.CharField(max_length = 256, default='')#the post message
    photo       = models.ImageField(blank=True, upload_to="post_images")
    file        = models.FileField(blank=True, upload_to="post_files")
    is_repost   = models.BooleanField(default=False, blank=True)#if this post is a repost 'shared'
    is_reply    = models.BooleanField(default=False, blank=True)#if this post is a reply to another one
    timestamp   = models.DateTimeField(auto_now_add=True)# date of creation
    last_update_time    = models.DateTimeField(auto_now_add=True)# date of last update

    objects     = PostManager()
    def __str__(self):#when we try to print(post_object)
        return 'Publication n° '+str(self.pk)+', de la part de: '+ str(self.user) +', dans le groupe: '+ str(self.group)

    def get_absolute_url(self):
        return reverse("post:detail", kwargs={'pk' : self.pk})

    def get_img_url(self):
        if self.photo:
            return self.photo.url
        return None

    def get_file_url(self):
        if self.file:
            return self.file.url
        return None

    def get_up_url(self):
        return reverse("api-post:up", kwargs={'pk' : self.pk})

    def get_create_comment_url(self):
        return reverse("api-comment:create", kwargs={'pk':self.pk})

    def get_replies(self):
        parent          = self
        post_queryset   = Post.objects.filter(
                                parent      = parent,
                                is_reply    = True
                                )
        return post_queryset

def post_save_receiver(sender, instance, created, *args, **kwargs):
    if created:
        user_regex          = r'@(?P<username>[\w.@+-]+)'
        usernames           = re.findall(user_regex, instance.content)
        #now we notify the user

        hash_regex          = r'#(?P<hashtag>[\w\d-]+)'
        hashtags            = re.findall(hash_regex, instance.content)
        #signal to create hashtags
        parsed_hashtags.send(sender = instance.__class__,hashtag_list = hashtags, post = instance)


post_save.connect(post_save_receiver, sender= Post)
