from django.contrib import admin

# Register your models here.

from .models import Post

admin.site.register(Post) #register app so it can be managed from admin site
