from django import forms

from .models import Post
from group.models import Group

USER_GROUPS=[]

class PostModelForm(forms.ModelForm):

    #the post message
    content = forms.CharField(
                        label="",#no label (by default we find the variable name "content")
                        help_text="",#text to help
                        widget=forms.Textarea( attrs={
                                'cols'          : "50", #size
                                'rows'          : "6", #size
                                'placeholder'   : 'Votre publication', #placeholder text
                                'style'         : 'resize : none' #non resizeable
                                })
                        )



    class Meta:
        model   = Post #we define the model

        fields  = [
        "content", #we define the fields (the field name must be the same as in the model)
        "group",
        "photo",
        "file"
        ]

class PostModelUpdateForm(forms.ModelForm):

    #the post message
    content = forms.CharField(
                        label="",#no label (by default we find the variable name "content")
                        help_text="",#text to help
                        widget=forms.Textarea( attrs={
                        'cols'          : "50", #size
                        'rows'          : "6", #size
                        'placeholder'   : 'Votre publication', #placeholder text
                        'style'         : 'resize : none' #non resizeable
                        }))

    # group = forms.

    class Meta:
        model   = Post #we define the model

        fields  = [
        "content", #we define the fields (the field name must be the same as in the model)
        ]

class PostModelCreateForm(forms.ModelForm):

    #the post message
    content = forms.CharField(
                        label="",#no label (by default we find the variable name "content")
                        help_text="",#text to help
                        widget=forms.Textarea( attrs={
                        'cols'          : "50", #size
                        'rows'          : "6", #size
                        'placeholder'   : 'Votre publication', #placeholder text
                        'style'         : 'resize : none' #non resizeable
                        }))

    group = forms.ChoiceField(choices=USER_GROUPS, label='')


    class Meta:
        model   = Post #we define the model
        fields  = [
        "content", #we define the fields (the field name must be the same as in the model)
        "group",
        "photo",
        "file"
        ]

    def __init__(self, user, *args, **kwargs):
        super(PostModelCreateForm, self).__init__(*args, **kwargs)
        auth_groups_queryset = Group.objects.all()
        for group in auth_groups_queryset:
            if user.profile in group.get_members():
                item    = [group.pk,group.name]
                USER_GROUPS.append(item)
