from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.views.generic import (
                                    ListView,
                                    DetailView,
                                    CreateView,
                                    DeleteView,
                                    UpdateView
                                    )
from django.urls import reverse_lazy

from .forms import PostModelForm, PostModelUpdateForm, PostModelCreateForm
from .mixins import FormUserNeededMixin, FormOwnerUserMixin, FormDeleteOwnerUserMixin
from .models import Post
from hashtag.signals import parsed_hashtags

import re

#the list view of our posts
class PostListView(LoginRequiredMixin, ListView):
    model       = Post                      #the model of data
    login_url   = '/login'
                                            #the template name by default post_list
    def get_queryset(self, *args, **kwargs):#override a method
        post_queryset    = Post.objects.all()#all posts
        queryContent     = self.request.GET.get("q",None)# get the query
        try:
            request_user_groups = self.request.user.profile.get_groups() #we get the request user
            post_queryset.filter(group__in=request_user_groups)#we filter posts which's group is in user's groups
        except:
            print('ERROR : Cannot get posts from groups where request_user is member in')

        if queryContent is not None:
            post_queryset    = post_queryset.filter(   #we filter as the query
                                Q(content__icontains=queryContent)|
                                Q(user__username=queryContent)
                                )
        return post_queryset   #data

    def get_context_data(self, *args, **kwargs):                               #override the method get_context
        context                     = super(PostListView,self).get_context_data()  #previous context
        context['post_form']        = PostModelCreateForm(user = self.request.user)                              #the form to create post
        context['create_post_url']  = reverse_lazy("post:create")             #we send data to PostCreateView
        context['fetch_post_url']   = reverse_lazy('api-post:list')            #where to fetch Data from API
        context['create_post_url']  = reverse_lazy('api-post:create')         #where to post Data to API when creating a post
        return context
#the create view of a post
class PostCreateView(LoginRequiredMixin, FormUserNeededMixin, CreateView):
     form_class    = PostModelCreateForm            #the form
     template_name = 'post/post_create.html/' #we must specify the templatefor CreateView
     success_url   = reverse_lazy("post:list")#on success of creating a post
     login_url     = '/login'

     def form_valid(self, form):
         if self.request.user.is_authenticated:
             form.instance.user = self.request.user
             return super(PostCreateView, self).form_valid(form)
         return self.form_invalid(form)

     def get_form_kwargs(self):
         kwargs = super(PostCreateView, self).get_form_kwargs()
         kwargs['user'] = self.request.user
         return kwargs



#the view to display only one post
class PostDetailView(LoginRequiredMixin, DetailView):
    model           = Post
    template_name   = ('post/post_detail.html')
    queryset        = Post.objects.all()
    login_url     = '/login'

    def get_object(self):
        post_id = self.kwargs.get('pk')
        post    = get_object_or_404(Post, pk=post_id)
        return post
    def get_context_data(self, *args, **kwargs):
        post_id                     = self.kwargs.get('pk')
        context                     = super(PostDetailView, self).get_context_data()
        context['fetch_post_url']   = reverse_lazy("api-post:detail", kwargs={'pk':post_id})
        context['update_url']       = reverse_lazy("post:update", kwargs={'pk':post_id})
        context['delete_url']       = reverse_lazy("post:delete", kwargs={'pk':post_id})
        context['repost_url']       = reverse_lazy("api-post:repost", kwargs={'pk':post_id})
        context['reply_url']        = reverse_lazy("api-post:reply", kwargs={'pk':post_id})
        return context

#the view to update a post
class PostUpdateView(LoginRequiredMixin, FormOwnerUserMixin, UpdateView):
    form_class          = PostModelUpdateForm
    template_name       = "post/post_update.html"
    queryset            = Post.objects.all()
    success_url         = "/post/list"
    login_url          = "/login"

    def get_object(self):
        post_id = self.kwargs.get('pk')
        post    = get_object_or_404(Post, pk=post_id)
        return post

    def form_valid(self, form):
        form.instance.last_update_time = datetime.now()

        hash_regex          = r'#(?P<hashtag>[\w\d-]+)'
        hashtags            = re.findall(hash_regex, form.instance.content)
        parsed_hashtags.send(sender = form.instance.__class__,hashtag_list = hashtags, post = form.instance) #w signal when updating a post containing a tag

        return super(PostUpdateView, self).form_valid(form)

#the delete view of a post
class PostDeleteView(LoginRequiredMixin, FormDeleteOwnerUserMixin, DeleteView):
    model       = Post #the model of post
    success_url = reverse_lazy('post:list') #when succes on delete go to success_url
    template_name = 'post/post_confirm_delete.html'#the template name of the view
    login_url     = '/login'
