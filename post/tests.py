from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from .models import Post
from account.models import Profile
from group.models import Group


UserModel = get_user_model()

class TestCasePostModel(TestCase):

    def setUp(self):
        userTestCase     = UserModel.objects.create(username = 'testUser')
        profileTestCase  = Profile.objects.create(
                                                user = userTestCase
                                                )
        groupTestCase = Group.objects.create(
                    name        = 'testGroup',
                    description = 'test'
                    )
        groupTestCase.moderators.add(userTestCase)
        groupTestCase.members.add(profileTestCase)


    def test_created_model(self):
        post    = Post.objects.first()
        profile = Profile.objects.first()
        user    = UserModel.objects.first()
        group   = Group.objects.first()

        objectTest  = Post.objects.create(
                                        user    = user,
                                        group   = group,
                                        content = 'testContent'
                                        )
        objectTest.uppers.add(user)

        self.assertTrue(objectTest.content == 'testContent')
        self.assertTrue(objectTest.id == 1)
        self.assertTrue(objectTest.user == user)
        self.assertTrue(objectTest.group == group)
        self.assertTrue(objectTest.uppers.first() == user)
        self.assertTrue(objectTest.group.moderators.first() == user)
        self.assertTrue(objectTest.group.members.first() == profile)
