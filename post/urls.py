
from django.urls import path
from django.views.generic.base import RedirectView

from .views import (
                PostDeleteView,
                PostDetailView,
                PostListView,
                PostCreateView,
                PostUpdateView
                )

app_name    = 'post'

urlpatterns = [
    path('list/', RedirectView.as_view(url='/home'), name='list'),#/
    path('<int:pk>/', PostDetailView.as_view(), name='detail'),#/post/detail
    path('<int:pk>/update', PostUpdateView.as_view(), name='update'),#/post/update
    path('<int:pk>/delete', PostDeleteView.as_view(), name='delete'),#/post/delete
    path('create/', PostCreateView.as_view(), name='create'),#/post/create
]
