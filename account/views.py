from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import (
                                    ListView,
                                    DetailView,
                                    CreateView,
                                    DeleteView,
                                    UpdateView
                                    )
from django.views.generic.edit import FormView

from .forms import UserRegisterForm, UserUpdateForm
from .mixins import FormOwnerUserMixin
from .models import Profile
from account.signals import user_register

UserModel = get_user_model()#the model of users (by django)

#the list view of our profiles
class ProfileListView(LoginRequiredMixin, ListView):
    model       = Profile #the profile model for data
    login_url          = "/login"

    def get_queryset(self, *args, **kwargs):#override a method
        queryset    = Profile.objects.all() #all profiles
        return queryset #data

#the detail view of one profile
class ProfileDetailView(LoginRequiredMixin, DetailView):
    template_name = 'account/profile_detail.html'#the template name of the view
    login_url          = "/login"


    def get_object(self):
        username = self.kwargs.get("username") # get username from url
        return get_object_or_404(UserModel,username__iexact = username) # return the user or 404

    def get_context_data(self, *args, **kwargs):
        username                  = self.kwargs.get('username')
        context                   = super(ProfileDetailView, self).get_context_data(*args, **kwargs)
        context['fetch_post_url'] = reverse_lazy("api-post:user", kwargs={'username':username})
        context['user_url']       = reverse_lazy("profile:detail", kwargs={'username':username})
        return context

#the user register view (we valid the form and create a user)
class UserRegisterView(FormView):
    form_class = UserRegisterForm
    template_name = "registration/user_register.html"
    success_url = "/login"
    #we override the form_valid method to create the user and save him
    #after this method the post_save is called and sends a signal (we catch the signal in the Profile model)
    def form_valid(self, form):
        username    = form.cleaned_data.get("username")
        first_name  = form.cleaned_data.get("first_name")
        last_name   = form.cleaned_data.get("last_name")
        email       = form.cleaned_data.get("email")
        password    = form.cleaned_data.get("password")
        group_id    = form.cleaned_data.get("group")
        photo           = form.cleaned_data.get("photo")
        status      = form.cleaned_data.get("status")
        new_user    = UserModel(
            username    = username,
            email       = email,
            first_name  = first_name,
            last_name   = last_name
        )
        new_user.set_password(password)
        new_user.save()
        user_register.send(sender=form.__class__, user=new_user, group_id= group_id, status=status, photo=photo)
        return super(UserRegisterView, self).form_valid(form)

class UserUpdateView(FormView):
    form_class          = UserUpdateForm
    template_name       = "account/user_update.html"
    queryset            = UserModel.objects.all()
    success_url         = "/post/list"
    login_url          = "/login"

    def form_valid(self, form):
        username        = self.request.user.username
        first_name      = form.cleaned_data.get("first_name")
        last_name       = form.cleaned_data.get("last_name")
        email           = form.cleaned_data.get("email")
        password        = form.cleaned_data.get("password")
        photo           = form.cleaned_data.get("photo")
        print(photo)
        # prev_password   = form.cleaned_data.get("previous_password")
        # group_id    = form.cleaned_data.get("group")
        status      = form.cleaned_data.get("status")
        user        = get_object_or_404(UserModel, username=username)

        if email:
            user.email          = email
        if first_name:
            user.first_name     = first_name
        if last_name:
            user.last_name      = last_name
        if status:
            user.profile.status = status
        if photo:
            user.profile.photo = photo
        if password:
            user.set_password(password)

        user.save()
        user.profile.save()
        return super(UserUpdateView, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(UserUpdateView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs
