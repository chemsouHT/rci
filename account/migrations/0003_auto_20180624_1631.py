# Generated by Django 2.0.6 on 2018-06-24 15:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_profile_groups'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='groups',
            field=models.ManyToManyField(related_name='members', to='group.Group'),
        ),
    ]
