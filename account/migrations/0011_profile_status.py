# Generated by Django 2.0.6 on 2018-07-02 10:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0010_auto_20180701_1233'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='status',
            field=models.CharField(blank=True, default='', max_length=64),
        ),
    ]
