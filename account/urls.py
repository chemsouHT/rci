
from django.urls import path

from .views import (
                ProfileListView,
                ProfileDetailView,
                UserUpdateView
                # UserRegisterView
                )


app_name    = 'account'

urlpatterns = [
    path('list/', ProfileListView.as_view(), name='list'),#/account/list
    path('update/', UserUpdateView.as_view(), name='update'),#/account/username/update
    # updateHashLinks() is hard coded url, if you change the below url you must change the updateHashLinks()
    path('<str:username>/', ProfileDetailView.as_view(), name='detail'),#/account/username
]
