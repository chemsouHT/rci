
from django.conf.urls.static import static
from django.contrib.auth import get_user_model
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.urls import reverse_lazy

from rest_framework import serializers

from account.models import Profile
from group.api.serializers import GroupModelSerializer

UserModel     = get_user_model() #this is the UserModel made bu django

#serialized User Model
class UserModelSerializer(serializers.ModelSerializer):
    profile_url     = serializers.SerializerMethodField()
    img_url         = serializers.SerializerMethodField()
    class Meta:
        model   = UserModel   # the model to get fields from
        fields  = [           # fields must be the same as the model fields/additional fields
        "id",
        "username",
        "email",
        "first_name",
        "last_name",
        "profile_url",
        "img_url"
        ]
    def get_profile_url(self, object):
        return reverse_lazy("profile:detail", kwargs={'username':object.username})

    def get_img_url(self, object):
        return object.profile.get_img_url()
        # return static('profile_pic/'+str(object.id)+'.png/')



class ProfileModelSerializer(serializers.ModelSerializer):
    user    = UserModelSerializer(read_only=True)

    class Meta:
        model   = Profile   # the model to get fields from
        fields  = [           # fields must be the same as the model fields/additional fields
        "user",
        "groups"
        ]

class UserModelUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model   = UserModel   # the model to get fields from
        fields  = [           # fields must be the same as the model fields/additional fields
        "username",
        "email",
        "first_name",
        "last_name",
        ]
