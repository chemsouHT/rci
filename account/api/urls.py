
from django.urls import path

from .views import (
                ProfileListAPIView,
                ProfileLastVisitAPIView
                )


app_name    = 'api-profile'

urlpatterns = [
    path('list/', ProfileListAPIView.as_view(), name='list'),#/api/profile/list
    path('last_visit/', ProfileLastVisitAPIView.as_view(), name='last_visit')#/api/profile/last_visit
]
