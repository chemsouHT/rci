from datetime import datetime

from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404

from rest_framework import generics, permissions
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from .pagination import ProfileDisplayPagination
from .serializers import ProfileModelSerializer, UserModelSerializer, UserModelUpdateSerializer
from account.models import Profile
from account.mixins import FormOwnerUserMixin

UserModel = get_user_model()

#the api view
class ProfileListAPIView(generics.ListAPIView):
    serializer_class    = ProfileModelSerializer           # the serializer class we made in .serializers
    permission_classes  = [permissions.IsAuthenticated] #user must be authenticated
    pagination_class    = ProfileDisplayPagination

    def get_queryset(self, *args, **kwargs):
        profile_queryset = Profile.objects.all().order_by('-timestamp')
        return profile_queryset

# the APIView to set the last visit time of a User
class ProfileLastVisitAPIView(APIView):
    def get(self, *args, **kwargs):
        profile     = self.request.user.profile
        time_now    = datetime.now()
        profile.update_last_visit(time_now)
        return Response({'last_visit':time_now})
