from django.conf import settings
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.db import models
from django.db.models.signals import post_save
from django.shortcuts import get_object_or_404
from django.urls import reverse

from .signals import user_register
from group.models import Group
from rci.settings.local import MEDIA_URL



# the model of Profile in database
class Profile(models.Model):
    user                = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='profile')
    groups              = models.ManyToManyField(Group, related_name='members', default=1)

    actif               = models.BooleanField(default=True)
    last_visit          = models.DateTimeField(auto_now_add=True)
    status              = models.CharField(max_length = 64, default='', blank=True)
    photo               = models.ImageField(upload_to='users_profile_pics', default='default.jpg', blank=True)
    timestamp           = models.DateTimeField(auto_now_add=True)# date of creation
    last_update_time    = models.DateTimeField(auto_now_add=True)# date of last update

    def __str__(self):
        return str(self.user)

    def get_groups(self, *args, **kwargs):
        return self.groups.all()

    def get_absolute_url(self):
        return reverse('profile:detail',kwargs={'username':self.user.username})

    def get_img_url(self):
        if self.photo:
            return self.photo.url
        return MEDIA_URL+'users_profile_pics/default.jpg'

    def update_last_visit(self, time):
        self.last_visit = time
        self.save()


def user_register_receiver(sender, user, group_id, status, photo, *args, **kwargs):
    new_profile,created = Profile.objects.get_or_create(
                                        user    = user,
                                        status  = status,
                                        photo   = photo
                                        )
    if created:
        group           = get_object_or_404(Group, pk=group_id)
        general_group   = get_object_or_404(Group, name='Général')
        new_profile.groups.add(general_group)
        new_profile.groups.add(group)
        new_profile.save()

user_register.connect(user_register_receiver)
