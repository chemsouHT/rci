from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from .models import Profile, Group
# Create your tests here.

UserModel = get_user_model()

class TestCaseAccountModel(TestCase):

    def setUp(self):
        userTestCase     = UserModel.objects.create(username = 'testUser')
        groupTestcase    = Group.objects.create(
                            name        = 'testGroup',
                            description = 'test'
                            )
        groupTestcase.moderators.add(userTestCase)

    def test_created_model(self):
        group  = Group.objects.first()
        objectTest = Profile.objects.create(
                    user    = UserModel.objects.first(),
                    )
        objectTest.groups.add(group)
        self.assertTrue(objectTest.user.username == 'testUser')
        self.assertTrue(objectTest.groups.first().name == 'testGroup')
        self.assertTrue(objectTest.id == 1)
        self.assertTrue(objectTest.actif == 1)
