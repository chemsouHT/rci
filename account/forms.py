from django import forms

from django.contrib.auth import get_user_model

from group.group_choices import ALL_GROUPS
from group.models import Group

User = get_user_model()

#Register a new user form
class UserRegisterForm(forms.Form):
    username                = forms.CharField(label  = "Nom d'utilisateur")
    last_name               = forms.CharField(label  = "Nom")
    first_name              = forms.CharField(label  = "Prénom")
    email                   = forms.EmailField(label = "E-mail")
    status                  = forms.CharField(label  = "Statut")
    photo                   = forms.ImageField(label = "Photo de profil", required=False)
    password                = forms.CharField(
                                        widget = forms.PasswordInput,
                                        label  = "Mot de passe"
                                        )
    password_confirmation   = forms.CharField(
                                        widget = forms.PasswordInput,
                                        label  = "Mot de passe [confirmation]"
                                        )
    group                   = forms.ChoiceField(
                                        choices = ALL_GROUPS,
                                        label   = "Département"
                                        )

    def clean_password_confirmation(self, *args, **kwargs):
        password                    = self.cleaned_data.get("password")
        password_confirmation       = self.cleaned_data.get("password_confirmation")
        if password_confirmation != password:
            raise forms.ValidationError("Passwords don't match")
        return password

    #a kind of mixin for the username
    def clean_username(self, *args, **kwargs):
        username                    = self.cleaned_data.get("username")
        if User.objects.filter(username__iexact = username).exists():
            raise forms.ValidationError("Nom d'utilisateur existe déja")
        else:
            username = username.replace('-','')
            username = username.replace('_','')
            if not username.isalnum():
                raise forms.ValidationError("Seulement les lettres, chiffres, '-', '_' sont autorisés")
        return username

class UserUpdateForm(forms.Form):
    last_name               = forms.CharField(label = "Nom", required=False)
    first_name              = forms.CharField(label = "Prénom", required=False)
    status                  = forms.CharField(label = "Statut", required=False)
    email                   = forms.EmailField(label = "E-mail", required=False)
    photo                   = forms.ImageField(label = "Photo de profil", required=False)
    password                = forms.CharField(
                                        widget = forms.PasswordInput,
                                        label  = "Nouveau mot de passe",
                                        required=False
                                        )
    password_confirmation   = forms.CharField(
                                        widget = forms.PasswordInput,
                                        label  = "Nouveau mot de passe [confirmation]",
                                        required=False
                                        )

    previous_password       = forms.CharField(
                                        widget = forms.PasswordInput,
                                        label  = "Ancien mot de passe [Requis en cas de nouveau mot de passe]",
                                        required=False
                                        )
    def clean_password_confirmation(self, *args, **kwargs):
        password                    = self.cleaned_data.get("password")
        password_confirmation       = self.cleaned_data.get("password_confirmation")
        if password is not None:
            if password_confirmation != password:
                raise forms.ValidationError("Les mots de passes ne sont pas identiques")
        return password

    def clean_previous_password(self, *args, **kwargs):
        password                    = self.cleaned_data.get("password")
        user                        = self.user
        user_password               = user.password
        previous_password           = self.cleaned_data.get("previous_password")
        print(password)
        if password != '':
            if not user.check_password(previous_password):
                raise forms.ValidationError("Mot de passe incorrect!")
        return previous_password

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

    #a kind of mixin for the username
