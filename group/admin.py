from django.contrib import admin

# Register your models here.

from django.contrib.auth.models import Group
from .models import Group as Group_

admin.site.unregister(Group)
admin.site.register(Group_)
