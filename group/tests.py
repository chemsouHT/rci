from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from account.models import Profile
from .models import Group

UserModel = get_user_model()

class TestCaseGroupModel(TestCase):

    def setUp(self):
        userTestCase     = UserModel.objects.create(username = 'testUser')
        profileTestCase  = Profile.objects.create(
                                                user = userTestCase
                                                )

    def test_created_model(self):
        objectTest = Group.objects.create(
                    name        = 'testGroup',
                    description = 'test'
                    )
        user        = UserModel.objects.first()
        profile     = Profile.objects.first()

        objectTest.moderators.add(user)
        objectTest.members.add(profile)

        self.assertTrue(objectTest.name == 'testGroup')
        self.assertTrue(objectTest.description == 'test')
        self.assertTrue(objectTest.id == 1)
        self.assertTrue(objectTest.moderators.first() == user)
        self.assertTrue(objectTest.members.first() == profile)
        self.assertTrue(objectTest.members.first().user == user)
