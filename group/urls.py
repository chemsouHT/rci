
from django.urls import path

from .views import (
                GroupDetailView
                )


app_name    = 'group'

urlpatterns = [
    path('<str:name>/', GroupDetailView.as_view(), name='detail'),#/group_name
]
