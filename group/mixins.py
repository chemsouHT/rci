
from django import forms
from django.forms.utils import ErrorList
from django.http import Http404


class FormUserNeededMixin(object):
     def form_valid(self, form):
         print('FormUserNeededMixin done!')
         if self.request.user.is_authenticated:
             form.instance.user=self.request.user
             return super(FormUserNeededMixin, self).form_valid(form)
         else:
             form._errors[forms.forms.NON_FIELD_ERRORS]=ErrorList(["user must be logged in to continue"])
             return self.form_invalid(form)


class FormOwnerUserMixin(FormUserNeededMixin, object):
     def form_valid(self, form):
         print('FormOwnerUserMixin done!')
         print('form user is   :',form.instance.user)
         print('request user is:',self.request.user)
         if form.instance.user==self.request.user:
             return super(FormUserNeededMixin, self).form_valid(form)
         else:
             form._errors[forms.forms.NON_FIELD_ERRORS]=ErrorList(["You're not allowed to change this"])
             return self.form_invalid(form)

class FormDeleteOwnerUserMixin(object):
    def get_object(self, queryset=None):
        tweet = super(FormDeleteOwnerUserMixin, self).get_object()
        if not tweet.user==self.request.user:
            raise Http404

        return tweet
