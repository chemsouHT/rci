from group.models import Group



ALL_GROUPS          = []
AUTH_USER_GROUPS    = []

all_groups_queryset = Group.objects.exclude(name='Général').values('pk','name')
for group in all_groups_queryset:
    item    = [group['pk'],group['name']]
    ALL_GROUPS.append(item)

auth_groups_queryset = Group.objects.all().values('pk','name')
for group in auth_groups_queryset:
    item    = [group['pk'],group['name']]
    AUTH_USER_GROUPS.append(item)
