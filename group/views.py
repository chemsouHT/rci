from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy, reverse

from .models import Group

from django.views.generic import (
                                ListView,
                                DetailView,
                                CreateView,
                                DeleteView
                                )
from post.forms import PostModelForm

#group detail view
class GroupDetailView(LoginRequiredMixin, DetailView):
    template_name = 'group/group_detail.html'
    login_url     = '/login'

    def get_object(self):
        group_name      = self.kwargs.get('name')
        return get_object_or_404(Group,name__iexact = group_name)

    def get_context_data(self, *args, **kwargs):                                        #override the method get_context
        group_name = self.kwargs.get('name')
        context                     = super(GroupDetailView,self).get_context_data(*args, **kwargs)    #previous context
        context['post_form']        = PostModelForm()                                   #the form to create post
        context['fetch_post_url']   = reverse_lazy('api-post:group', kwargs={'name':group_name})#where to fetch Data from API
        context['create_post_url']  = reverse_lazy('api-post:create')               #where to post Data to API when creating a post
        return context
