from rest_framework import pagination



class GroupDisplayPagination(pagination.PageNumberPagination):
    page_size           = 10
    page_query_param    = 'page_size'
    page_page_size      = 1000
