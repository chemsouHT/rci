
from django.urls import reverse_lazy

from rest_framework import serializers

from group.models import Group


#serialized class for Group model
class GroupModelSerializer(serializers.ModelSerializer):
    group_url   = serializers.SerializerMethodField()
    class Meta:
        model  = Group   # the model to get fields from
        fields = [      # fields must be the same as the model fields/additional fields
        'id',
        'moderators',
        'name',
        'description',
        'timestamp',
        'group_url'
        ]

    def get_group_url(self, object):
        return reverse_lazy("group:detail", kwargs={'name':object.name})

class GroupModelForCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model  = Group   # the model to get fields from
        fields = [      # fields must be the same as the model fields/additional fields
        'name',
        ]

    
