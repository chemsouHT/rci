# from django
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404

from rest_framework import generics, permissions
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from .pagination import GroupDisplayPagination
from .serializers import GroupModelSerializer
from group.models import Group

UserModel  = get_user_model()

#the api view
class GroupListAPIView(generics.ListAPIView):
    serializer_class    = GroupModelSerializer           # the serializer class we made in .serializers
    permission_classes  = [permissions.IsAuthenticated] #user must be authenticated
    pagination_class    = GroupDisplayPagination

    def get_queryset(self, *args, **kwargs):
        group_queryset = Group.objects.all().order_by('-timestamp')
        return group_queryset

#add a user to a group api view
class GroupAddUserAPIView(APIView):
    permission_classes  = [permissions.IsAuthenticated]
    def get(self, request, format = None, *args, **kwargs):
        group_name  = self.kwargs.get('name')
        username    = self.kwargs.get('username')
        group       = get_object_or_404(Group, name=group_name)
        user        = get_object_or_404(UserModel, username=username)
        if request.user.is_authenticated:
            if request.user in group.get_moderators():
                if user in group.get_join_asks():
                    if user.profile not in group.get_members():
                        user_member_in = Group.objects.add_user(user, group)
                        return Response({"user_member_in" : user_member_in})
                    return Response({"message" : "l'utilisateur est déja membre"}, status = 400 )
                return Response({"message" : "l'utilisateur n'a pas demandé l'ajout"}, status = 400 )
            return Response({"message" : "Vous n'êtes pas modérateur de ce groupe"}, status = 400 )
        return Response({"message" : "Vous êtes Anonyme"}, status = 400 )

class GroupRemoveDemandUserAPIView(APIView):
    permission_classes  = [permissions.IsAuthenticated]
    def get(self, request, format = None, *args, **kwargs):
        group_name  = self.kwargs.get('name')
        username    = self.kwargs.get('username')
        group       = get_object_or_404(Group, name=group_name)
        user        = get_object_or_404(UserModel, username=username)
        if request.user.is_authenticated:
            if request.user in group.get_moderators():
                user_member_in = Group.objects.remove_demand_user(user, group)
                return Response({"user_member_in" : user_member_in})
            return Response({"message" : "Vous n'êtes pas modérateur de ce groupe"}, status = 400 )
        return Response({"message" : "Vous êtes Anonyme"}, status = 400 )

class GroupRemoveUserAPIView(APIView):
    permission_classes  = [permissions.IsAuthenticated]
    def get(self, request, format = None, *args, **kwargs):
        group_name  = self.kwargs.get('name')
        username    = self.kwargs.get('username')
        group       = get_object_or_404(Group, name=group_name)
        user        = get_object_or_404(UserModel, username=username)
        if request.user.is_authenticated:
            if request.user in group.get_moderators():
                if user not in group.get_moderators():
                    if user.profile in group.get_members():
                        user_member_in = Group.objects.remove_user(user, group)
                        return Response({"user_member_in" : user_member_in})
                    return Response({"message" : "L'utilisateur n'est pas membre du groupe"})
                return Response({"message" : "L'utilisateur est un modérateur du groupe"})
            return Response({"message" : "Vous n'êtes pas modérateur de ce groupe"}, status = 400 )
        return Response({"message" : "Vous êtes Anonyme"}, status = 400 )

class GroupDemandUserAPIView(APIView):
    permission_classes  = [permissions.IsAuthenticated]
    def get(self, request, format = None, *args, **kwargs):
        group_name  = self.kwargs.get('name')
        username    = self.kwargs.get('username')
        group       = get_object_or_404(Group, name=group_name)
        user        = get_object_or_404(UserModel, username=username)
        if request.user.is_authenticated:
            if user.profile not in group.get_members():
                if user.profile not in group.get_join_asks():
                    user_member_in = Group.objects.ask_join_user(user, group)
                    return Response({"user_member_in" : user_member_in})
                return Response({"message" : "Une demande d'ajout est déja en cours de traitement"})
            return Response({"message" : "L'utilisateur est déja membre du groupe"})
        return Response({"message" : "Vous êtes Anonyme"}, status = 400 )

class GroupDemandModeratorAPIView(APIView):
    permission_classes  = [permissions.IsAuthenticated]
    def get(self, request, format = None, *args, **kwargs):
        group_name  = self.kwargs.get('name')
        username    = self.kwargs.get('username')
        group       = get_object_or_404(Group, name=group_name)
        user        = get_object_or_404(UserModel, username=username)
        if request.user.is_authenticated:
            if user.profile in group.get_members():
                if user not in group.get_moderators():
                    if user not in group.get_moderators_asks():
                        user_moderator_of = Group.objects.ask_moderators_user(user, group)
                        return Response({"user_moderator_of" : user_moderator_of})
                    return Response({"message" : "Une demande de modération est déja en cours de traitement"})
                return Response({"message" : "L'utilisateur est déja modérateur de ce groupe"})
            return Response({"message" : "L'utilisateur n'est même pas membre du groupe"})
        return Response({"message" : "Vous êtes Anonyme"}, status = 400 )

class GroupAddModeratorAPIView(APIView):
    permission_classes  = [permissions.IsAuthenticated]
    def get(self, request, format = None, *args, **kwargs):
        group_name  = self.kwargs.get('name')
        username    = self.kwargs.get('username')
        group       = get_object_or_404(Group, name=group_name)
        user        = get_object_or_404(UserModel, username=username)
        if request.user.is_authenticated:
            if request.user in group.get_moderators():
                if user in group.get_moderators_asks():
                    if user.profile not in group.get_moderators():
                        user_moderator_of = Group.objects.add_moderator(user, group)
                        return Response({"user_moderator_of" : user_moderator_of})
                    return Response({"message" : "l'utilisateur est déja modérateur de ce groupe"}, status = 400 )
                return Response({"message" : "l'utilisateur n'a pas demandé l'ajout"}, status = 400 )
            return Response({"message" : "Vous n'êtes pas modérateur de ce groupe"}, status = 400 )
        return Response({"message" : "Vous êtes Anonyme"}, status = 400 )

class GroupRemoveDemandModeratorAPIView(APIView):
    permission_classes  = [permissions.IsAuthenticated]
    def get(self, request, format = None, *args, **kwargs):
        group_name  = self.kwargs.get('name')
        username    = self.kwargs.get('username')
        group       = get_object_or_404(Group, name=group_name)
        user        = get_object_or_404(UserModel, username=username)
        if request.user.is_authenticated:
            if request.user in group.get_moderators():
                user_moderator_of = Group.objects.remove_demand_moderator(user, group)
                return Response({"user_moderator_of" : user_moderator_of})
            return Response({"message" : "Vous n'êtes pas modérateur de ce groupe"}, status = 400 )
        return Response({"message" : "Vous êtes Anonyme"}, status = 400 )
