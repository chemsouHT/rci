
from django.urls import path

from .views import (
                GroupListAPIView,
                GroupAddModeratorAPIView,
                GroupAddUserAPIView,
                GroupDemandModeratorAPIView,
                GroupDemandUserAPIView,
                GroupRemoveDemandUserAPIView,
                GroupRemoveDemandModeratorAPIView,
                GroupRemoveUserAPIView,
                )


app_name    = 'api-group'

urlpatterns = [
    path('list/', GroupListAPIView.as_view(), name='list'),#/api/group/list
    path('add/<str:username>/to/<str:name>/', GroupAddUserAPIView.as_view(), name='add_user'),#/api/group/add/username/to/group_name
    path('add/<str:username>/to_moderate/<str:name>/', GroupAddModeratorAPIView.as_view(), name='add_moderator'),#/api/group/add/username/to_moderate/group_name
    path('remove_demand/<str:username>/to/<str:name>/', GroupRemoveDemandUserAPIView.as_view(), name='remove_demand'),#/api/group/remove_demand/username/to/group_name
    path('remove_demand/<str:username>/to_moderate/<str:name>/', GroupRemoveDemandModeratorAPIView.as_view(), name='remove_demand_moderate'),#/api/group/remove_demand/username/to_moderate/group_name
    path('remove/<str:username>/from/<str:name>/', GroupRemoveUserAPIView.as_view(), name='remove_user'),#/api/group/remove/username/from/group_name
    path('demand/<str:username>/to/<str:name>/', GroupDemandUserAPIView.as_view(), name='demand'),#/api/group/demand/username/to/group_name
    path('demand/<str:username>/to_moderate/<str:name>/', GroupDemandModeratorAPIView.as_view(), name='demand_moderate'),#/api/group/demand/username/to_moderate/group_name
]
