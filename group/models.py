from django.conf import settings
from django.db import models
from django.urls import reverse


#the Manager of the Group Model
class GroupManager(models.Manager):

    def add_user(self, user, group):
        group.join_asks.remove(user)
        user.profile.groups.add(group)
        return user.profile in group.get_members()

    def remove_demand_user(self, user, group):
        group.join_asks.remove(user)
        return user.profile in group.get_members()

    def remove_user(self, user, group):
        user.profile.groups.remove(group)
        return user.profile in group.get_members()

    def ask_join_user(self, user, group):
        group.join_asks.add(user)
        return user.profile in group.get_members()

    def ask_moderators_user(self, user, group):
        group.moderators_asks.add(user)
        return user in group.get_moderators()

    def add_moderator(self, user, group):
        group.moderators_asks.remove(user)
        group.moderators.add(user)
        return user in group.get_moderators()

    def remove_demand_moderator(self, user, group):
        group.moderators_asks.remove(user)
        return user in group.get_moderators()


#Model group in database
class Group(models.Model):
    moderators          = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='moderator_of', default=1)#le modérateur du groupe
    join_asks           = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='ask_to_join', default=1, blank=True)#le les demandes d'accès au groupe
    moderators_asks     = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='ask_to_moderate', default=1, blank=True)#le les demandes de modérer le groupe

    name        = models.CharField(max_length=50, default='', unique=True)#name of the group
    description = models.CharField(max_length=200, default='')#description of the group
    timestamp   = models.DateTimeField(auto_now_add=True)# date of creation
    last_update_time    = models.DateTimeField(auto_now_add=True)# date of last update

    objects     = GroupManager()

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse("group:detail", kwargs={'name':self.name})

    def get_members(self):#members is the related name of member_in in Profile
        return self.members.all()

    def get_moderators(self):
        return self.moderators.all()

    def get_join_asks(self):
        return self.join_asks.all()

    def get_moderators_asks(self):
        return self.moderators_asks.all()

    def has_moderator(self, user):
        return user in self.get_moderators()
