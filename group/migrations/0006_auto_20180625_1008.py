# Generated by Django 2.0.6 on 2018-06-25 09:08

from django.conf import settings
from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('group', '0005_remove_group_members'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.RemoveField(
            model_name='group',
            name='moderator',
        ),
        migrations.AddField(
            model_name='group',
            name='moderator',
            field=models.ManyToManyField(default=1, related_name='moderator_of', to=settings.AUTH_USER_MODEL),
        ),
    ]
