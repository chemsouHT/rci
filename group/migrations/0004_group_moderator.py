# Generated by Django 2.0.6 on 2018-06-24 13:46

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('group', '0003_auto_20180624_1442'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='moderator',
            field=models.OneToOneField(default='1', on_delete='cascade', related_name='moderator_of', to=settings.AUTH_USER_MODEL),
        ),
    ]
