"""rci URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include


from .views import home_view, SearchView
from account.views import UserRegisterView
from post.views import PostListView

urlpatterns = [
    path('', home_view, name='home'),
    path('home/', PostListView.as_view(), name='post-list'),
    path('admin/', admin.site.urls),
    path('search/', SearchView.as_view(), name="search"),
    path('register/', UserRegisterView.as_view(), name="register"),
    path('post/', include('post.urls', namespace='post')),
    path('profile/', include('account.urls', namespace='profile')),
    path('group/', include('group.urls', namespace='group')),
    path('tag/', include('hashtag.urls', namespace='hashtag')),
    path('comment/', include('comment.urls', namespace='comment')),
    path('api/post/',include('post.api.urls', namespace='api-post')),
    path('api/profile/',include('account.api.urls', namespace='api-profile')),
    path('api/group/',include('group.api.urls', namespace='api-group')),
    path('api/comment/',include('comment.api.urls', namespace='api-comment')),
    path('', include('django.contrib.auth.urls')),

    # path('api/hashtag/',include('hashtag.api.urls', namespace='api-hashtag')),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
