
function listenToGroup() {

  //handles accept user delete user delete request to join a group
  $('.group-access-management-btn').on("click", function(event){
    event.preventDefault();

    var this_ = $(this)
    var instance  = $(this).parent();
    var groupName = instance.attr("group-name");
    var username  = instance.attr("user-username");
    var fetchUrl  = instance.attr('action-url');
    $.ajax({
       type    : "GET",
       url     : fetchUrl,
       data    : {
         'name'     : groupName,
         'username' : username
       },
       success : function(data){
         // location.reload();
         if (this_.attr('type')=='group-demand-access')
            location.reload();
         else
            this_.parent().parent().parent().parent().slideUp(200);
         popUpMessage('Opération réussie.')
       },
       error   : function(data){
         console.log("ERROR:CH0x600 while managing group access");
         console.log("data :",data.status, data.statusText);
       }
    });
  });

  //handles accept user delete user delete request to join a group
  $('.group-moderate-management-btn').on("click", function(event){
    event.preventDefault();

    var this_ = $(this)
    var instance  = $(this).parent();
    var groupName = instance.attr("group-name");
    var username  = instance.attr("user-username");
    var fetchUrl  = instance.attr('action-url');
    $.ajax({
       type    : "GET",
       url     : fetchUrl,
       data    : {
         'name'     : groupName,
         'username' : username
       },
       success : function(data){
         // location.reload();
         if (this_.attr('type')=='group-demand-access'){
           popUpMessage('Demande envoyée.',"Demande d'ajout de modérateur",2000);
           setTimeout(function(){
             location.reload();
           },2000)
         }
         else{
           this_.parent().parent().parent().parent().slideUp(200);
           popUpMessage('Opération réussie avec succès.',"Demande d'ajout de modérateur",2000);
         }
       },
       error   : function(data){
         console.log("ERROR:CH0x700 while Adding user to a group");
         console.log("data :",data.status, data.statusText);
       }
    });
  });

  $('#show-members-btn').on("click", function(event){
    event.preventDefault();

    membersModal = $('#members-modal');
    membersModal.modal({});
  });

  $('#show-members-request-btn').on("click", function(event){
    event.preventDefault();

    membersModal = $('#ask-join-modal');
    membersModal.modal({});
  });

  $('#show-moderators-request-btn').on("click", function(event){
    event.preventDefault();

    moderatorsAskModal = $('#ask-moderate-modal');
    moderatorsAskModal.modal({});
  });
}
