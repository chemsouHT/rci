
//listen to all interactions with the post detail page
function listenToPost(){
  var repostUtl;
  var replyUrl;


  $('.repost-btn').on("click", function(event){
    event.preventDefault();

    this_ =$(this);
    var repostModalForm = $('#repost-modal');
    var postToRepostContainer    = repostModalForm.find('#post-to-repost');
    var postToRepost             = $(this_.attr('post-to-repost-content'));

    repostModalForm.modal({});//launch the modal to repost
    postToRepostContainer.text("");
    postToRepostContainer.append(postToRepost);

    repostUrl = this_.attr('repost-url');
  });

  $('#submit_repost_form').on("click", function(event){
    event.preventDefault();
    var selectedGroup = $('#repost-group-select option:selected').val();
    $.ajax({
       type    : "GET",
       url     : repostUrl,
       data    : {
         'group' : selectedGroup
       },
       success : location.reload(),
       error   : function(data){
         console.log("ERROR:CH0x0111 while RePosting");
         console.log("data :",data.status, data.statusText);
       }
    });
  });

  $('.reply-btn').on("click", function(event){
    event.preventDefault();

    this_ =$(this);
    var replyModalForm          = $('#reply-modal');
    var postToReplyContainer    = replyModalForm.find('#post-to-reply-to');
    var postToReplyImg          = replyModalForm.find('#post-to-reply-to-img');
    var postToReplyToContent    = $(this_.attr('post-to-reply-to-content'));
    var postToReplyToImg        = $(this_.attr('post-to-reply-to-img'));
    var replyContent            = $('#reply-content');
    var postUserUsername        = this_.attr('post-user');

    replyModalForm.modal({});//launch the modal to reply
    postToReplyContainer.text("");
    postToReplyContainer.append(postToReplyToContent);
    postToReplyImg.append(postToReplyToImg);

    replyUrl = this_.attr('reply-url');
    replyContent.val('@'+postUserUsername+' ');
  });


  $('#submit_reply_form').on("click", function(event){
    event.preventDefault();
    var content = $('#reply-content').val();
    $.ajax({
       type    : "GET",
       url     : replyUrl,
       data    : {
         'content' : content
       },
       success : location.reload(),
       error   : function(data){
         console.log("ERROR:CH0x0112 while Replying");
         console.log("data :",data.status, data.statusText);
       }
    });
  });



}
