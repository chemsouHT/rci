from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import render, reverse
from django.views import View

from group.models import Group
from hashtag.models import Hashtag

User = get_user_model()

def home_view(request):

    return render(request,"home.html",{})
    # return render(request,"home.html",context)

class SearchView(LoginRequiredMixin, View):
    login_url   = '/login'
    def get(self, request, *args, **kwargs):
        query                   = request.GET.get("q")
        users_queryset    = None
        if query is not None:
            users_queryset    = User.objects.filter(
                                Q(username__icontains=query)
                                )
        groups_queryset    = None
        if query is not None:
            groups_queryset    = Group.objects.filter(
                                Q(name__icontains=query)
                                )
        hashtags_queryset    = None
        if query is not None:
            hashtags_queryset  = Hashtag.objects.filter(
                                Q(tag__icontains=query)
                                )

        context = {
            "users"                 : users_queryset,
            "groups"                : groups_queryset,
            "tags"                  : hashtags_queryset,
            "fetch_post_url"        : reverse("api-post:list"),
            }
        return render(request, "search.html", context)
