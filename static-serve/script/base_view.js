$(document).on("ready", function(){

  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });

  listenToNavBar();
  listenToSearchBar();
  $('.back-to-top').click(function(){
    console.log('click');
    $('html, body').animate({scrollTop : 0},'fast');
    return false;
  });

  
})
