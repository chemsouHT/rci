//Listen and handle acount/profile events
function listenToProfile(){
  form                   = $('#user-form');
  previous_password      = form.find('#id_previous_password');
  previous_passwordLabel = previous_password.parent().siblings('label');

  previous_password.prop("disabled",true);
  previous_passwordLabel.addClass("disabled-label");

  $('#id_password').on("change paste keyup", function(){
    this_             = $(this);
    previous_password = form.find('#id_previous_password');

    if(this_.val()){
      previous_password.prop("disabled",false);
      previous_passwordLabel.removeClass("disabled-label");

    }
    else{
      previous_password.prop("disabled",true);
      previous_passwordLabel.addClass("disabled-label");
    }

  });
}


function listenToNavBar(){

  $('#logout-btn').on('click', function(event){

    fetchUrl = $(this).attr('last-visit-url');

    $.ajax({
       type    : "GET",
       url     : fetchUrl,
       success : function(data){
         popUpMessage('Bye.')
       },
       error   : function(data){
         console.log("ERROR:CH0x0502 while Logging out");
         console.log("data :",data.status, data.statusText);
       }
    });

  });

}
