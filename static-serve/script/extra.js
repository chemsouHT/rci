messageBox=$('#message-box');
messageBoxConfirmation=$('#message-box-confirmation');

function setPostFormRemainingChars(){

    var postFormTextMaxLength = 250;
    var postCreateForm        = $('.post_form_class');
    var submitPostFormButton  = postCreateForm.find(".submit_form");
    var postFormContentHTML   = postCreateForm.find('textarea');

    postFormContentHTML.keyup(function(event){

      var postFormTextValue   = $(this).val();
      var postRemainingChars  = postFormTextMaxLength - postFormTextValue.length;
      var postCharsText       = postFormTextValue.length + "/" + postFormTextMaxLength;

      var spanCharsLeft = postCreateForm.find(".post-chars-left");
        //updating style
        if(postRemainingChars > 0){
          spanCharsLeft.removeClass('red-color');
          spanCharsLeft.removeClass('grey-color');
        }else if (postRemainingChars == 0){
          spanCharsLeft.removeClass('red-color');
          spanCharsLeft.addClass('grey-color');
        }else{
          spanCharsLeft.addClass('red-color');
          spanCharsLeft.removeClass('grey-color');
        }

        //updating button enabled/disabled
        submitPostFormButton.prop('disabled', postRemainingChars < 0);
        //updating remaining chars text
        spanCharsLeft.text(postCharsText) ;
    });




}

function popUpMessage(message, title, timeout=1000){
    var msgField    = messageBox.find("#message-box-message");
    var titleField  = messageBox.find("#box-title");
    msgField.text(message);
    titleField.text(title);
    messageBox.modal({});
    setTimeout(function(){
      messageBox.modal('hide');
    },timeout)
  }

function popUpMessageConfirmation(message, title){
    var msgField    = messageBoxConfirmation.find("#message-box-message");
    var titleField  = messageBoxConfirmation.find("#box-title");
    msgField.text(message);
    titleField.text(title);
    messageBoxConfirmation.modal({});
  }

function setPostCreateDisplay(){
  //styles
  $('#id_photo').attr('style', 'display:none;');
  $('#id_photo').addClass('input');
  $('#id_photo').wrap("<label class='btn btn-default btn-file'> <i class='fa fa-photo fa-2x'></i> </label>");
  $('#id_file').attr('style', 'display:none;');
  $('#id_file').addClass('input');
  $('#id_file').wrap("<label class='btn btn-default btn-file'> <i class='fa fa-file fa-2x'></i> </label>");

  //divs
  $('#div_id_photo').attr("style", "display: inline-block;");
  $('#div_id_file').attr("style", "display: inline-block;");

  //labels
  var labelGroup   = $('#div_id_group').find('.control-label');
  var labelPhoto   = $('#div_id_photo').find('.control-label');
  var labelFile    = $('#div_id_file').find('.control-label');
  labelGroup.hide();
  labelPhoto.hide();
  labelFile.hide();

  //listeners
  $('.input').on("change", function(){
    var this_  = $(this);

    icon = this_.siblings('i');
    icon.addClass('blue-color');
  })

}
