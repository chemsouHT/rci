
var postList = [];                //we put posts here before parsing them
var commentList = [];             //we put comments here before parsing them
var postContainerHTMLItem;        //the html container where we're gonna attach posts
var commentContainerHTMLItem;     //the html container of comments of on post
var fetchListPostUrl;             // the url to fetch data from
var fetchDetailPostUrl;           // the url to fetch data from
var fetchCommentPartialUrl;       // the first part of the fetch comment url (needs the post id at the end)
var createPostUrl;                // the url to post data to create a post
var DisplayMode;                  // what mode of display (group, list, detail post ...)
var groupName;                    //The name of the group
var requestUser         = $('.settings').attr("request-user"); // the request user from Body HTML in Base (class "settings")
var postCountBtn        = $('#post-search-btn'); // When we select the Post category in search results (we need it's badge to display count)
var query;                        //the query of search
var nextPostsUrl        = null;
var previousPostsUrl    = null;
var nextCommentsUrl     = null;
var previousCommentUrl  = null;
var MaxCommentChars     = 150;


//must be called in the beginning of the calling script for listing posts and comments
function initAjaxListVariables(dataContainerId, fetchUrl, createUrl) {
  postContainerHTMLItem = $('#' + dataContainerId);
  fetchListPostUrl      = fetchUrl;
  createPostUrl         = createUrl;

}


//must be called in the beginning for displaying groups
function initAjaxGroupVariables(groupName, Display='group-display') {
  groupName         = groupName;
  DisplayMode       = Display;
}

//must be called in the beginning for displaying groups
function initAjaxPostVariables(Display='post-display') {
  DisplayMode       = Display;
}
//get the query q as regex from the url
function getParameterByName(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
  }


//update the hashtag links and break lines
function updateHashLinks(text){
    var hashTagRegex  = /(^|\s)#([\w\d-]+)/g;
    var userTagRegex  = /(^|\s)@([\w\d-]+)/g;
    var urlRegex      = /([a-z]+\:\/+)([^\/\s]*)([a-z0-9\-@\^=%&;\/~\+]*)[\?]?([^ \#]*)#?([^ \#]*)/ig;


    var newHtml1      = text.replace(hashTagRegex, "$1<a href = '/tag/$2/'>#$2</a>");
    var newHtml2      = newHtml1.replace(userTagRegex, "$1<a href = '/profile/$2/'>@$2</a>");
    var newHtml3      = newHtml2.replace(urlRegex, function(url) {
        return '<a href="' + url + '">' + url + '</a>';
    });
    var response = newHtml3.replace(/\n/g, "<br />");
  return response;
}

//format the user to get an html content
function formatUserHTML(username, userDetailUrl, userImgUrl){

    return"<a href = '"+ userDetailUrl +"'>"+
            "<div class=\"row\">"+
              "<div class=\"profile-header-container\">" +
            		"<div class=\"profile-header-img col-sm-3 \">" +
                  "<img class=\"img-circle\" src='"+ userImgUrl +"' />"+
                  "<div class=\"rank-label-container\">"+
                    "<span class=\"label label-default rank-label\">"+ username +"</span>"+
                  "</div>"+
                "</div>"+
              "</div>"+
            "</div>"+
          "</a>";
}

//format a postObject to a html rendered
function formatPostHTML(postObject){
    var postUserUsername    = postObject.user.username;
    var postUserUrl         = postObject.user.profile_url || '#';
    var postUserImgUrl      = postObject.user.img_url || '#';

    var postId              = postObject.id;
    var postAge             = postObject.age;
    var postContent         = postObject.content;
    var postParent          = postObject.parent;
    var postIsRepost        = postObject.is_repost;
    var postIsReply         = postObject.is_reply;
    var postUps             = postObject.ups;
    var postDidUserUp       = postObject.did_user_up;
    var postGroupName       = postObject.group.name || '';
    var postImg             = postObject.photo;
    var postImgUrl          = postObject.img_url || '#';
    var postFile            = postObject.file;
    var postFileUrl         = postObject.file_url || '#';
    var postGroupUrl        = postObject.group.group_url || '#';
    var postInfoUrl         = postObject.info_url || '#';
    var postUpUrl           = postObject.up_url || '#';
    var postCreateCommentUrl= postObject.create_comment_url || '#';

    var nextCommentsBtnHTML = "<a id='more-comments-btn"+ postId +"' post-id='"+ postId +"' class='more-comments-btn'>  </a>";
    var groupDetailsHTML  ="<a class='link-btn hvr-shadow' href='"+ postGroupUrl +"'> "+ postGroupName +" </a>"+ "  ";
    var postParentText = "";
    var postImgHTML = "";
    var postFileHTML = "";

    if(postImg)
      postImgHTML="<a class='post-img-btn' href='#'> <img class='post-img' src='"+ postImgUrl +"'> </a>";

    if(postFile)
      postFileHTML=
      "</br>"+
      "<a id='detail-post-file' file-url="+ postFileUrl +"  class='post-file-btn hvr-shadow' href='#'>"+
      "<i class='fa fa-file-text-o fa-3x'></i>"+
      "</a></br>";

    var postUpTextHtml ="<a href='#' class='post-up-btn link-btn hvr-icon-up' up-post-url='"+ postUpUrl +"'>  <i class='fa fa-arrow-circle-o-up hvr-icon'></i></a> ( <span>"+ postUps +"</span> ) ";
    if (postDidUserUp)
      postUpTextHtml = "<a href='#' class='post-up-btn link-btn hvr-icon-down' up-post-url='"+ postUpUrl +"'>  <i class='fa fa-arrow-circle-o-down hvr-icon'></i></a> ( <span>"+ postUps +"</span> ) ";

    if(postParent){
      if(postIsRepost)
        postParentText = "<span class='grey-color' id ='repost-parent' >Partagé | auteur:  "+
        "<a href='"+postParent.user.profile_url+"'>"+ postParent.user.username +"</a>"+
        "   Il y a  "+ postParent.age +"  |"+
        "<a href='"+ postParent.group.group_url +"'> "+ postParent.group.name +" </a></br>"+
        "</span> <hr>";
      else if (postIsReply)
        postParentText = "<span class='grey-color' id ='reply-parent' >Réponse à la "+
          "<a href='"+ postParent.info_url +"'> publication </a>"+
          "Il y a "+ postParent.age+
        "</span></br>";
    }
    if(DisplayMode == 'group-display') groupDetailsHTML="";


    var postFormattedHTML =
    "<div style='display:none;'>"+
      "<div  class=\"row \">" +
        "<div class=\"col-sm-3\">" +
          formatUserHTML(postUserUsername, postUserUrl, postUserImgUrl)+
        "</div>" +
          "<div class=\"col-sm-8  \" id='post'>" +
            postParentText+
            "<span id = 'post-content' class='break-words' > " +postContent + " </span></br>" +
            postImgHTML+" </br> "+postFileHTML+"</br>"+
            "Il y a "+ postAge +" </br> "+
            groupDetailsHTML+
             postUpTextHtml+

            "<button class='btn comment-show-btn rounded hvr-icon-down' post-id="+ postId +"> Commentaires <i class='fa fa-chevron-down hvr-icon'></i> </button>"+
            "<row>"+
              "<form class='comment-form' method='POST' post-id="+ postId +" style='display:none;' id='comment-form-of-post"+ postId +"' create-comment-url ='"+ postCreateCommentUrl +"'>"+
                "<hr>"+
                "<textarea cols='50' rows='3' placeholder='Votre commentaire' class='comment-content' style='resize:none' id='comment-content-of-post"+ postId +"'>"+
                "</textarea> </br>"+
                "<input  class='btn btn-primary comment-btn' post-id="+ postId +" id='submit-comment-of-post"+ postId +"' type='submit' value='Commenter' style='' />"+
              "</form>"+
              "<div class=' ' id='comments-of-post"+ postId +"'>"+

              "</div>"+
              "</br>"+
              nextCommentsBtnHTML+
            "</row>"+
          "</div>"+
          "<div class='col-sm-1'>"+
            "<div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>"+
              "<ul class='nav navbar-nav navbar-right'>"+
                "<li class='dropdown' style='overflow:visible'>"+
                  "<button  type='button' class='dropdown-toggle transparent-background hvr-icon-grow' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'><i class='fa fa-bars hvr-icon'></i></button>"+
                  "<ul class='dropdown-menu'>"+
                    "<li style='width:auto;'><a href='"+ postInfoUrl +"'><i class='fa fa-info-circle '></i>          Détails </a></li>"+
                    // "<li role='separator' class='divider'></li>"+

                  "</ul>"+
                "</li>"+
              "</ul>"+
            "</div>"+
          "</div>"+
        "</div>" +
        " <hr/>"+
      "</div> "+
    "</div>";

    return postFormattedHTML
  }


//attach one post to the container on prepend or append
function attachPost(postObject, prepend=false){
    var postFormattedHTML = formatPostHTML(postObject);

    postFormattedHTML     = updateHashLinks(postFormattedHTML);
    // postContainerHTMLItem.hide();
    if(prepend)
      postContainerHTMLItem.prepend(postFormattedHTML);
    else
      postContainerHTMLItem.append(postFormattedHTML);
    postContainerHTMLItem.show(200);


  }

//attach all posts from postList to the conainer
function parsePost(){
    if (postList == 0){
      if(DisplayMode!="post-display")
        postContainerHTMLItem.append( "<h4>Aucune publication trouvée.</h4>");
    }else {
      // postContainerHTMLItem.hide();
      $.each(postList, function(key,object){
        var postKey      =key;
        var postFormattedHTML = formatPostHTML(object);
        postFormattedHTML     = updateHashLinks(postFormattedHTML);
        postContainerHTMLItem.append(postFormattedHTML);
      });

      postContainerHTMLItem.find('div:first-child').show('fast', function showNext(){
        $(this).next('div').show('fast',showNext);
      });

    }
  }

//get data from fetchDataUrl, if success call successFunction(data)
function fetchPostData(fetchDataUrl,successFunction){
  $.ajax({
    type       : "GET",
    url        : fetchDataUrl,
    contentType: 'application/json',
    data       :{'q':query},
    success    : function(data){
      successFunction(data.results);//do the successfunction with the data
      nextPostsUrl     = data.next;//get next data URL (pagination used)
      previousPostsUrl = data.previous;//get previous data URL (pagination used)
      postCountBtn.find('.badge').text(data.count) //set the badge of post research
    },
    error      : function(data){
      console.log("ERROR:CH0x0401 while fetching Posts");
      console.log("data :",data.status, data.statusText);
    }
  });

}

//format a commentObject to a html rendered
function formatCommentHTML(commentObject){
    var commentUserUsername    = commentObject.user.username;
    var commentUserUrl         = commentObject.user.profile_url;
    var commentUserImgUrl      = commentObject.user.img_url;

    var commentId              = commentObject.id;
    var commentAge             = commentObject.age;
    var commentContent         = commentObject.content;
    var commentUps             = commentObject.ups;
    var commentDidUserUp       = commentObject.did_user_up;
    var commentDeleteUrl       = commentObject.confirm_delete_url;
    var commentUpdateUrl       = commentObject.update_url;
    var commentUpUrl           = commentObject.up_url;

    var commentOwnerOptionsHTML= "";
    var commentUpTextHtml ="<a href='#' class='post-up-btn link-btn hvr-icon-up' up-post-url='"+ commentUpUrl +"'>  <i class='fa fa-arrow-circle-o-up hvr-icon'></i></a> ( <span>"+ commentUps +"</span> ) ";
    if (commentDidUserUp)
      commentUpTextHtml = "<a href='#' class='post-up-btn link-btn hvr-icon-down' up-post-url='"+ commentUpUrl +"'>  <i class='fa fa-arrow-circle-o-down hvr-icon'></i></a> ( <span>"+ commentUps +"</span> ) ";

    if (requestUser == commentUserUsername){
      commentOwnerOptionsHTML=
          "<div class='collapse navbar-collapse top-right-corner' id='bs-example-navbar-collapse-1'>"+
              "<ul class='nav navbar-nav navbar-right'>"+
                "<li class='dropdown' style='overflow:visible'>"+
                  "<button  type='button' class='dropdown-toggle transparent-background hvr-icon-grow' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'><i class='fa fa-bars hvr-icon'></i></button>"+
                  "<ul class='dropdown-menu'>"+
                    "<li style='width:auto;'><a href='"+commentDeleteUrl + "'><i class='fa fa-trash '></i>  Supprimer </a></li>"+
                    "<li style='width:auto;'><a href='"+commentUpdateUrl + "'><i class='fa fa-pencil-square-o '></i>  Modifier </a></li>"+
                    // "<li role='separator' class='divider'></li>"+

                  "</ul>"+
                "</li>"+
              "</ul>"+
            "</div>";

    }
    var commentFormattedHTML   =
    "<div style='display:none;'>"+
      "<hr>"+
      "<div class=\"row \">" +
        "<div class=\"col-sm-3 \">" +
                  formatUserHTML(commentUserUsername, commentUserUrl, commentUserImgUrl)+
        "</div>" +
        "<div class=\"col-sm-8 col-sm-offset-1  \" id='post'>" +
          "<span id = 'comment-content' > " +commentContent + " </span></br>" +
          "Il y a "+commentAge + " | "+
          commentUpTextHtml+
          commentOwnerOptionsHTML+
          "</br>" +
        "</div>" +
      "</div> "+
    "</div>";


    return commentFormattedHTML;
  }


//attach one comment to the container on prepend or append
function attachComment(commentObject, prepend=false){
    var commentFormattedHTML = formatCommentHTML(commentObject);
    commentFormattedHTML     = updateHashLinks(commentFormattedHTML);
    // commentContainerHTMLItem.hide();
    if(prepend)
      commentContainerHTMLItem.prepend(commentFormattedHTML);
    else
      commentContainerHTMLItem.append(commentFormattedHTML);
    commentContainerHTMLItem.find('div:first-child').show('fast', function showNext(){
      $(this).next('div').show('fast',showNext);
    });


  }
//attach all comments from commentList to the commentContainer
function parseComment(){
    if (commentList == 0){
      // commentContainerHTMLItem.text( "Aucun commentaire trouvé.");
    }else {
      $.each(commentList, function(key,object){
        var postKey      =key;
        var commentFormattedHTML = formatCommentHTML(object);
        commentFormattedHTML     = updateHashLinks(commentFormattedHTML);
        commentContainerHTMLItem.append(commentFormattedHTML);
      });

      commentContainerHTMLItem.find('div:first-child').show('fast', function showNext(){
        $(this).next('div').show('fast',showNext);
      });

    }
  }
//get data from fetchDataUrl, if success call successFunction(data)
function fetchCommentData(fetchDataUrl, postId, successFunction){
  $.ajax({
    type       : "GET",
    url        : fetchDataUrl,
    contentType: 'application/json',
    success    : function(data){
      successFunction(data.results);//do the successfunction with the data
      nextCommentsUrl = data.next;//get next data URL (pagination used)
      previousCommentsUrl = data.previous;//get previous data URL (pagination used)
      if(nextCommentsUrl)
        $('#more-comments-btn'+postId).text('Plus');
      else
        $('#more-comments-btn'+postId).text('');
    },
    error      : function(data){
      console.log("ERROR:CH0x0402 while fetching Comments");
      console.log("data :",data.status, data.statusText);
    }
  });
}

//update the post list
function updatePostListDisplay(dataUrl){
  fetchPostData(dataUrl, function(data){
    postList  = data;
    parsePost();
  });
}

//init comments
function showCommentList(postId){
  var fetchCommentsUrl  = '/api/comment/post/'+postId;/////////////////////////////////////////////////////////////////////////////////////////////////////////a changer
  fetchCommentData(fetchCommentsUrl, postId, function(data){
    commentList  = data;
    parseComment();

  });
}

//when we click more comments
function showMoreCommentList(postId){
  if(nextCommentsUrl)
  fetchCommentData(nextCommentsUrl, postId, function(data){
    commentList  = data;
    parseComment();

  });
}

// when we submit comment form
function commentThePost(commentContent, dataUrl){
    $.ajax({
       method     : "GET",
       url        : dataUrl,
       data       : {
         'content' : commentContent,
          },//data contains our new comment
       success    : function(data){
          attachComment(data,prepend=true);
       },
       error      : function(data){
         console.log("ERROR:CH0x0102 while fetching after commenting");
         console.log("data :",data.status, data.statusText);
       }
     });

}

//init the post list
function initPostListDisplay(){
  query   = getParameterByName('q');
  fetchPostData(fetchListPostUrl, function(data){
    postList  = data;
    parsePost();

  });


  //catch the event when we submit the post create form
  $(document.body).on("submit", ".post_form_class",function(event){
      event.preventDefault();// no more event catching => no redirect for action_url of the form

      this_ = $(this);
      var formData  = new FormData(this);//we get the serialized typed data from the form
      $.ajax({
         method       : "POST",
         url          : createPostUrl,
         data         : formData,//data contains our new post
         processData  :false,
         contentType  :false,
         success      : function(data){
           this_.find("input[type=text], textarea").val("");
           // attachPost(data, prepend = true);//add at the beginning prepend
           location.reload(true); // we reload instead of attaching
         },
         error        : function(data){
           console.log("ERROR:CH0x0101 while fetching after creation form submit (create post)");
           console.log("data :",data.status, data.statusText);
         }
       });

  });

  //When we reach the end we fetch more Data
  $(window).on("scroll", function() {
  	var docHeight          = $(document).height();
  	var windowHeight       = $(window).outerHeight(true);
    var WindowscrollTop    = $(window).scrollTop();
    var scrollBottomPos    = windowHeight + WindowscrollTop;
  	if (scrollBottomPos == docHeight) {
  	    // when scroll to bottom of the page
        if(nextPostsUrl)
          updatePostListDisplay(nextPostsUrl);//update Display by fetching then attaching ancient posts
  	}
  });

  $(document.body).on("click", ".comment-show-btn",function(event){
    event.preventDefault();
    var this_             = $(this);
    var postId            = this_.attr('post-id');
    var commentForm       = $('#comment-form-of-post'+postId);
    commentForm.show();
    commentContainerHTMLItem = $('#comments-of-post'+postId);
    showCommentList(postId);
    this_.hide();
  });

  $(document.body).on("submit", ".comment-form",function(event){
    event.preventDefault();

    var this_             = $(this);
    var postId            = this_.attr('post-id');
    var commentForm       = $('#comment-form-of-post'+postId);
    var commentContent    = $('#comment-content-of-post'+postId);
    var commentUrl        = commentForm.attr('create-comment-url');
    var content           = commentContent.val();
    // var formData          = this_.serialize();
    commentContent.val("");
    commentContainerHTMLItem = $('#comments-of-post'+postId);
    commentThePost(content, commentUrl)

  });


  $(document.body).on("click", ".more-comments-btn",function(event){
    event.preventDefault();

    var postId = $(this).attr('post-id');
    showMoreCommentList(postId);
  });

  $(document.body).on("click", ".post-up-btn", function(event){
    event.preventDefault();

    var this_ = $(this);
    fetchUrl  = this_.attr("up-post-url");

    $.ajax({
      type    :"GET",
      url     :fetchUrl,
      success :function(data){
        if (data["upped"]){
          this_.removeClass("hvr-icon-up");
          this_.addClass("hvr-icon-down");
          this_.find('i').removeClass('fa-arrow-circle-o-up');
          this_.find('i').addClass('fa-arrow-circle-o-down');
          span = this_.next('span');
          ups  = Number(span.text());
          ups++;
          span.text(ups);
          // this_.find('a').text('Up')
        }
        else{
          this_.removeClass("hvr-icon-down");
          this_.addClass("hvr-icon-up");
          this_.find('i').removeClass('fa-arrow-circle-o-down');
          this_.find('i').addClass('fa-arrow-circle-o-up');
          span = this_.next('span');
          ups  = Number(span.text());
          ups--;
          span.text(ups);
          // this_.find('a').text('unUp')
        }

      },
      error   :function(data){
        console.log("ERROR:CH0x0215 while Toggle Up Post");
        console.log("data :",data.status, data.statusText);
      }
    });
  });


  $(document.body).on("click", ".comment-up-btn", function(event){
    event.preventDefault();

    var this_ = $(this);
    fetchUrl  = this_.attr("up-comment-url");

    $.ajax({
      type    :"GET",
      url     :fetchUrl,
      success :function(data){
        if (data["upped"]){
          this_.removeClass("hvr-icon-up");
          this_.addClass("hvr-icon-down");
          this_.find('i').removeClass('fa-arrow-circle-o-up');
          this_.find('i').addClass('fa-arrow-circle-o-down');
          span = this_.next('span');
          ups  = Number(span.text());
          ups++;
          span.text(ups);
        }
        else{
          this_.removeClass("hvr-icon-down");
          this_.addClass("hvr-icon-up");
          this_.find('i').removeClass('fa-arrow-circle-o-down');
          this_.find('i').addClass('fa-arrow-circle-o-up');
          span = this_.next('span');
          ups  = Number(span.text());
          ups--;
          span.text(ups);
        }
      },
      error   :function(data){
        console.log("ERROR:CH0x0225 while Toggle Up Comments");
        console.log("data :",data.status, data.statusText);
      }
    });
  });


  $(document.body).on("click", ".post-img-btn", function(event){
    event.preventDefault();

    var this_    = $(this);
    var imgUrl   = this_.find('img').attr("src");
    var imgModal = $('#post-img-modal');
    var imgHTML  = $('#post-img-display');

    imgModal.modal({});
    imgHTML.attr("src", imgUrl);

  });

  $(document.body).on("click", ".post-file-btn", function(event){
    event.preventDefault();

    var this_    = $(this);
    var fileUrl   = this_.attr("file-url");
    var fileModal = $('#post-file-modal');
    var fileHTMLDisplay  = fileModal.find('#post-file-display');
    var fileHTMLDownload = fileModal.find('#post-file-download');

    if(fileUrl.split('.').pop()=='pdf'){
      fileHTMLDownload.hide();
      fileHTMLDisplay.show();
      fileHTMLDisplay.attr("src", fileUrl);
    }
    else{
      fileHTMLDownload.show();
      fileHTMLDisplay.hide();
      fileHTMLDownload.find('#post-file-download-link').attr("href", fileUrl);
    }
    fileModal.modal({});

  });

  // Add slideDown animation to Bootstrap dropdown when expanding.
  $(document).on('show.bs.dropdown', '.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown('fast');
  });

  // Add slideUp animation to Bootstrap dropdown when collapsing.
  $(document).on('hide.bs.dropdown', '.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp('fast');
  });

  //Limit the comment characters
  $(document.body).on("change paste keydown", ".comment-content", function(event){
    this_         = $(this);
    var submitBtn = this_.siblings('input');
    if(this_.val().length > MaxCommentChars)
      submitBtn.prop('disabled', true);
    else
      submitBtn.prop('disabled', false);
  });

}
