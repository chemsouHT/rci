function listenToSearches(){


  $(document).on("click", "#profile-search-btn", function(event){
    event.preventDefault();
    $(".search-content").hide();
    $("#user-content").show('normal');

  });

  $(document).on("click", "#post-search-btn", function(event){
    event.preventDefault();
    $(".search-content").hide();
    $("#post-content").fadeIn();


  });

  $(document).on("click", "#hashtag-search-btn", function(event){
    event.preventDefault();
    $(".search-content").hide();
    $("#hashtag-content").show('normal');


  });

  $(document).on("click", "#group-search-btn", function(event){
    event.preventDefault();
    $(".search-content").hide();
    $("#group-content").show('normal');


  });
};

function listenToSearchBar(){
  var submitSearchBtn = $('#submit-search-btn');

  submitSearchBtn.val('');
  submitSearchBtn.prop('disabled', true);

  $('#search_input').on('change paste keyup', function(){

    if($(this).val())
      submitSearchBtn.prop('disabled', false)
    else
      submitSearchBtn.prop('disabled', true)

  });
}
