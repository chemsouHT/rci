
from django.urls import reverse_lazy
from django.utils.timesince import timesince


from rest_framework import serializers

from account.api.serializers import UserModelSerializer
from comment.models import Comment
from post.api.serializers import PostModelSerializer


#serialized class for Comments model (no post linked)
class CommentModelFamilySerializer(serializers.ModelSerializer):
    user                        = UserModelSerializer(read_only=True)
    class Meta:
        model  = Comment   # the model to get fields from
        fields = [      # fields must be the same as the model fields/additional fields
        'id',
        'user',
        'content',
        'timestamp',
        'last_update_time',

        ]


#serialized class for Comment model
class CommentModelSerializer(serializers.ModelSerializer):
    user    = UserModelSerializer(read_only=True)
    post    = PostModelSerializer(read_only=True)
    timestamp_display           = serializers.SerializerMethodField()
    last_update_time_display    = serializers.SerializerMethodField()
    age                         = serializers.SerializerMethodField()
    ups                         = serializers.SerializerMethodField()
    did_user_up                 = serializers.SerializerMethodField()
    up_url                      = serializers.SerializerMethodField()
    confirm_delete_url          = serializers.SerializerMethodField()
    update_url                  = serializers.SerializerMethodField()
    class Meta:
        model  = Comment   # the model to get fields from
        fields = [      # fields must be the same as the model fields/additional fields
        'id',
        'user',
        'post',
        'content',
        'uppers',
        'ups',
        'did_user_up',
        'timestamp',
        'timestamp_display',
        'last_update_time',
        'last_update_time_display',
        'age',
        'confirm_delete_url',
        'update_url',
        'up_url'

        ]
    def get_timestamp_display(self, object):
        return object.timestamp.strftime("%b %d %I:%M %p ")#format timestamp

    def get_last_update_time_display(self, object):
        return object.last_update_time.strftime("%b %d %I:%M %p ")#format last_update_time

    def get_age(self, object):
        return timesince(object.timestamp)

    def get_confirm_delete_url(self, object):
        return reverse_lazy("comment:delete", kwargs={'pk':object.id})

    def get_update_url(self, object):
        return reverse_lazy("comment:update", kwargs={'pk':object.id})

    def get_up_url(self, object):
        return reverse_lazy("api-comment:up", kwargs={'pk':object.id})

    def get_ups(self, object):
        return object.uppers.all().count()

    def get_did_user_up(self, object):
        request = self.context.get("request")
        if(request):
            request_user = request.user
            if request_user.is_authenticated:
                return request_user in object.uppers.all()
        return False
