from datetime import datetime

from django.shortcuts import get_object_or_404

from rest_framework import generics, permissions
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from .pagination import CommentDisplayPagination
from .serializers import CommentModelSerializer, CommentModelFamilySerializer
from comment.models import Comment
from post.models import Post

#the api view
class CommentDetailAPIView(generics.RetrieveAPIView): #we're gonna list the children and parents
    serializer_class    = CommentModelSerializer
    permission_classes  = [permissions.IsAuthenticated]

    def get_object(self, *args, **kwargs):
        pk = self.kwargs.get('pk')
        object = get_object_or_404(Comment, pk = pk)
        return object

class CommentDetailFamilyAPIView(generics.ListAPIView): #we're gonna list the children and parents
    serializer_class    = CommentModelSerializer
    permission_classes  = [permissions.IsAuthenticated]
    pagination_class    = CommentDisplayPagination

    def get_queryset(self, *args, **kwargs):
        comment_id     = self.kwargs.get('pk')
        comment     = get_object_or_404(Comment, pk= comment_id)
        queryset    = comment.get_family().order_by('-timestamp')
        return queryset

class CommentListPostAPIView(generics.ListAPIView):
    serializer_class    = CommentModelSerializer
    permission_classes  = [permissions.IsAuthenticated]
    pagination_class    = CommentDisplayPagination

    def get_queryset(self, *args, **kwargs):
        post_id     = self.kwargs.get('pk')
        post        = get_object_or_404(Post, pk= post_id)
        queryset    = Comment.objects.get_comments(post).order_by('-timestamp')
        return queryset



class CommentUpdateAPIView(generics.UpdateAPIView):
    serializer_class    = CommentModelSerializer
    permission_classes  = [permissions.IsAuthenticated]
    queryset            = Comment.objects.all()

    def perform_update(self, serializer):
        serializer.save(last_update_time = datetime.now())

class CommentCreateAPIView(APIView):
    permission_classes  = [permissions.IsAuthenticated]    #user must be authenticated

    def get(self, request, pk, format = None): # we create a comment and return an http response
        post_queryset = Post.objects.filter(pk = pk)
        if post_queryset.exists() and post_queryset.count() == 1 : # if it's a one element queryset
            if request.user.is_authenticated:
                content = self.request.GET.get("content",None)
                comment = Comment.objects.do_comment(request.user, post_queryset.first(), content)
                if comment :
                    data = CommentModelSerializer(comment).data #we serialize the comment to bes unserstood by the API
                    return Response(data)
                return Response({"message" : "Impossible de créer ce commentaire avec ce contenu"}, status = 400 )
            return Response({"message" : "not allowed-Anonymous-"}, status = 400 )
        return Response({"message" : "No post to comment"}, status = 400 )

class CommentUpToggleAPIView(APIView):
    permission_classes  = [permissions.IsAuthenticated]
    def get(self, request, pk, format = None):
        comment_queryset = Comment.objects.filter(pk = pk)
        if request.user.is_authenticated:
            is_upped = Comment.objects.up_toggle(request.user, comment_queryset.first())
            return Response({"upped" : is_upped})
        return Response({"message" : "Utilisateur Anonyme"}, status = 400 )
