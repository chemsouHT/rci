
from django.urls import path

from .views import (
                CommentCreateAPIView,
                CommentDetailAPIView,
                CommentDetailFamilyAPIView,
                CommentListPostAPIView,
                CommentUpdateAPIView,
                CommentUpToggleAPIView
                )


app_name    = 'api-comment'

urlpatterns = [
    path('<int:pk>/', CommentDetailAPIView.as_view(), name='detail'),#/api/comment/int
    path('<int:pk>/update', CommentUpdateAPIView.as_view(), name='update'),#/api/comment/int/update
    path('<int:pk>/up', CommentUpToggleAPIView.as_view(), name='up'),#/api/comment/int/up
    path('<int:pk>/all', CommentDetailFamilyAPIView.as_view(), name='detail-all'),#/api/comment/int/all
    path('post/<int:pk>/', CommentListPostAPIView.as_view(), name='post'),#/api/comment/int/post
    path('<int:pk>/create/', CommentCreateAPIView.as_view(), name='create'),#/api/comment/int/create
]
