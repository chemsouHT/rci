
from django.urls import path

from .views import (
                CommentListView,
                CommentDetailView,
                CommentDeleteView,
                CommentCreateView,
                CommentUpdateView
                )


app_name    = 'comment'

urlpatterns = [
    path('list/', CommentListView.as_view(), name='list'),#/comment/list
    path('create/', CommentCreateView.as_view(), name='create'),#/comment/create
    path('<int:pk>/', CommentDetailView.as_view(), name='detail'),#/comment/int
    path('<int:pk>/delete', CommentDeleteView.as_view(), name='delete'),#/comment/int/delete
    path('<int:pk>/update', CommentUpdateView.as_view(), name='update'),#/comment/int/update
]
