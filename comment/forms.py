from django import forms

from .models import Comment

class CommentModelForm(forms.ModelForm):

    #the comment message
    content = forms.CharField(
                        label="",#no label (by default we find the variable name "content")
                        help_text="",#text to help
                        widget=forms.Textarea( attrs={
                        'cols'          : "50", #size
                        'rows'          : "6", #size
                        'placeholder'   : 'Votre publication', #placeholder text
                        'style'         : 'resize : none' #non resizeable
                        }))


    class Meta:
        model   = Comment #we define the model
        fields  = [#we define the fields (the field name must be the same as in the model)
        "content",
        "post"
        ]

class CommentModelUpdateForm(forms.ModelForm):

    #the comment message
    content = forms.CharField(
                        label="",#no label (by default we find the variable name "content")
                        help_text="",#text to help
                        widget=forms.Textarea( attrs={
                        'cols'          : "50", #size
                        'rows'          : "6", #size
                        'placeholder'   : 'Votre publication', #placeholder text
                        'style'         : 'resize : none' #non resizeable
                        }))


    class Meta:
        model   = Comment #we define the model
        fields  = [#we define the fields (the field name must be the same as in the model)
        "content",
        ]
