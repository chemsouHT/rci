from django.conf import settings
from django.db import models
from django.db.models.signals import post_save

from hashtag.signals import parsed_hashtags
from post.models import Post

import re


#the manager of the comment model
class CommentModelManager(models.Manager):
    def do_comment(self, user, post, content):
        new_comment = None
        if content:
            new_comment = self.model(
                            post    = post,
                            user    = user,
                            content = content
                            )
            new_comment.save()
        return new_comment

    def get_comments(self, post):
        comment_queryset    = Comment.objects.filter(post = post)
        return comment_queryset

    def get_posts_by_user(self, user):
        usercomments_queryset       = Comment.objects.filter(user__username=user)
        usercomments_posts_ids      = usercomments_queryset.values('post')
        usercomments_posts_queryset = Post.objects.filter(id__in=usercomments_posts_ids)
        return usercomments_posts_queryset

    def up_toggle(self, user, comment):
        upped_by_user = user in comment.uppers.all()

        if upped_by_user:#if it's already upped so we will unlike
            comment.uppers.remove(user)
            return False # we unlike
        else :
            comment.uppers.add(user)
            return True # we like


#the model of comments in database
class Comment(models.Model):
    post        = models.ForeignKey(Post, default=1, on_delete=models.CASCADE)#posts related to a tag
    user        = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE)#the owner when we delete him, all his comments are deleted
    uppers      = models.ManyToManyField(settings.AUTH_USER_MODEL,related_name='upped_comment', blank=True)#users who upped this comment


    content     = models.CharField(max_length = 150, default='')#the comment message
    timestamp   = models.DateTimeField(auto_now_add=True)# date of creation
    last_update_time    = models.DateTimeField(auto_now_add=True)# date of last update

    objects = CommentModelManager()
    def __str__(self):
        return 'Commentaire n° '+str(self.pk)+', de la part de: '+ str(self.user) +', dans la publication n°: '+ str(self.post.pk) +', dans le groupe: '+ str(self.post.group)
        #for a comment as self, return all comments of the same parent
    def get_family(self, **kwargs):
        parent = self.post
        return Comment.objects.filter(post=parent)

    def get_up_url(self):
        return reverse("api-comment:up", kwargs={'pk' : self.pk})

def post_save_receiver(sender, instance, created, *args, **kwargs):
    if created:
        user_regex          = r'@(?P<username>[\w.@+-]+)'
        usernames           = re.findall(user_regex, instance.content)
        #now we notify the user

        hash_regex          = r'#(?P<hashtag>[\w\d-]+)'
        hashtags            = re.findall(hash_regex, instance.content)
        #signal to create hashtags
        parsed_hashtags.send(sender = instance.__class__,hashtag_list = hashtags, post = instance.post)


post_save.connect(post_save_receiver, sender= Comment)
