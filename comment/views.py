from datetime import datetime

from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404
from django.views.generic import (
                                    ListView,
                                    DetailView,
                                    CreateView,
                                    DeleteView,
                                    UpdateView
                                    )
from django.urls import reverse_lazy

from .forms import CommentModelForm, CommentModelUpdateForm
from .mixins import FormDeleteOwnerUserMixin, FormOwnerUserMixin, FormUserNeededMixin
from .models import Comment
from hashtag.signals import parsed_hashtags

import re

UserModel = get_user_model()#the model of users (by django)

#the list view of our comments
class CommentListView(LoginRequiredMixin, ListView):
    model       = Comment #the comment model for data
    login_url   = "/login"

    #template by default comment_list.html
    def get_queryset(self, *args, **kwargs):#override a method
        queryset    = Comment.objects.all() #all comments
        return queryset #data


#the create view of a comment
class CommentCreateView(LoginRequiredMixin, FormUserNeededMixin, CreateView):#becareful the mother classes order
    form_class    = CommentModelForm                #the form
    template_name = 'comment/comment_create.html/'  #we must specify the templatefor CreateView
    success_url   = reverse_lazy("post:list")    #on success of creating a post
    login_url     = "/login"
    #we must override form_valid to set the request user as the owner
    #it's done in FormUserNeededMixin



#the detail view of one comment
class CommentDetailView(LoginRequiredMixin, DetailView):
    template_name = 'comment/comment_detail.html'#the template name of the view
    login_url          = "/login"
    queryset      = Comment.objects.all()
    def get_object(self):
        id = self.kwargs.get("pk") # get id from url
        return get_object_or_404(Comment,pk = id) # return the user or 404

#the delete view of a comment
class CommentDeleteView(LoginRequiredMixin, FormDeleteOwnerUserMixin, DeleteView):
    model           = Comment #the model of comment
    success_url     = reverse_lazy('post:list') #when succes on delete go to success_url
    template_name   = 'comment/comment_confirm_delete.html'#the template name of the view
    login_url       = "/login"



#the update view of comments
class CommentUpdateView(LoginRequiredMixin, FormOwnerUserMixin, UpdateView):
    form_class      = CommentModelUpdateForm         #the form of update is the same as create
    template_name   = 'comment/comment_update.html'  #the template name
    queryset        = Comment.objects.all()         #all comments
    success_url     = reverse_lazy('post:list')
    login_url       = "/login"

    def form_valid(self, form):
        form.instance.last_update_time = datetime.now()

        hash_regex          = r'#(?P<hashtag>[\w\d-]+)'
        hashtags            = re.findall(hash_regex, form.instance.content)
        parsed_hashtags.send(sender = form.instance.__class__,hashtag_list = hashtags, post = form.instance.post) #w signal when updating a post containing a tag

        return super(CommentUpdateView, self).form_valid(form)
