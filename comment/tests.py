from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from account.models import Profile
from group.models import Group
from .models import Comment, Post


UserModel = get_user_model()

class TestCaseCommentModel(TestCase):

    def setUp(self):
        userTestCase     = UserModel.objects.create(username = 'testUser')
        profileTestCase  = Profile.objects.create(
                                                user = userTestCase
                                                )
        groupTestCase = Group.objects.create(
                    name        = 'testGroup',
                    description = 'test'
                    )
        groupTestCase.moderators.add(userTestCase)
        groupTestCase.members.add(profileTestCase)
        postTestCase  = Post.objects.create(
                                            user    = userTestCase,
                                            group   = groupTestCase,
                                            content = 'testContent'
                                            )

    def test_created_model(self):
        post    = Post.objects.first()
        profile = Profile.objects.first()
        user    = UserModel.objects.first()
        group   = Group.objects.first()

        objectTest      = Comment.objects.create(
                                        post    = post,
                                        user    = user,
                                        content = 'testComment'
                                        )

        self.assertTrue(objectTest.content == 'testComment')
        self.assertTrue(objectTest.id == 1)
        self.assertTrue(objectTest.post == post)
        self.assertTrue(objectTest.user == user)
        self.assertTrue(objectTest.post.user == user)
        self.assertTrue(objectTest.post.group == group)
        self.assertTrue(objectTest.user.profile == profile)
        self.assertTrue(objectTest.post.group.moderators.first() == user)
        self.assertTrue(objectTest.post.group.members.first() == profile)
