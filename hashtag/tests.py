from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from .models import Hashtag
from account.models import Profile
from group.models import Group
from post.models import  Post


UserModel = get_user_model()

class TestCaseTagModel(TestCase):

    def setUp(self):
        userTestCase     = UserModel.objects.create(username = 'testUser')
        profileTestCase  = Profile.objects.create(
                                                user = userTestCase
                                                )
        groupTestCase = Group.objects.create(
                    name        = 'testGroup',
                    description = 'test'
                    )
        groupTestCase.moderators.add(userTestCase)
        groupTestCase.members.add(profileTestCase)
        postTestCase  = Post.objects.create(
                                            user    = userTestCase,
                                            group   = groupTestCase,
                                            content = 'testContent'
                                            )

    def test_created_model(self):
        post    = Post.objects.first()
        profile = Profile.objects.first()
        user    = UserModel.objects.first()
        group   = Group.objects.first()

        objectTest      = Hashtag.objects.create(
                                        tag = 'testTag'
                                        )
        objectTest.posts.add(post)

        self.assertTrue(objectTest.tag == 'testTag')
        self.assertTrue(objectTest.id == 1)
        self.assertTrue(objectTest.posts.first() == post)
        self.assertTrue(objectTest.posts.first().user == user)
        self.assertTrue(objectTest.posts.first().group == group)
        self.assertTrue(objectTest.posts.first().user.profile == profile)
        self.assertTrue(objectTest.posts.first().group.moderators.first() == user)
        self.assertTrue(objectTest.posts.first().group.members.first() == profile)
