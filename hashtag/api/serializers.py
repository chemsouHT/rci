
from django.urls import reverse_lazy


from rest_framework import serializers

from hashtag.models import Hashtag
from post.api.serializers import PostModelSerializer


#serialized class for Post model
class HashtagModelSerializer(serializers.ModelSerializer):
    posts     = PostModelSerializer()
    class Meta:
        model  = Hashtag   # the model to get fields from
        fields = [      # fields must be the same as the model fields/additional fields
        'tag',
        'posts',
        'timestamp',
        ]
