from django.db import models
from django.shortcuts import reverse

from .signals import parsed_hashtags
from comment.models import Comment
from post.models import Post


#hashtag model in database
class Hashtag(models.Model):
    posts       = models.ManyToManyField(Post, related_name='tags', default=1)#posts related to a tag

    tag         = models.CharField(max_length = 150, default='')#the tag itself
    timestamp   = models.DateTimeField(auto_now_add=True)# date of creation
    last_update_time    = models.DateTimeField(auto_now_add=True)# date of last update

    def __str__(self):
        return str(self.tag)

    def get_absolute_url(self):
        return reverse("hashtag:detail", kwargs={'tag':self.tag})

    def get_posts(self):
        post_queryset       = self.posts.all()
        # tag                 = self.tag
        # for post in post_queryset:
        #     if '#'+tag not in str(post.content):
        #         comments = Comment.objects.get_comments(post)
        #         for comment in comments:
        #             if '#'+tag not in str(comment.content):
        #                 post_queryset = post_queryset.exclude(pk=post.pk)
        return post_queryset


def parsed_hashtags_receiver(sender, hashtag_list, *args, **kwargs):
    if len(hashtag_list) > 0 :
        post = kwargs.get('post')
        for tag_item in hashtag_list:
            new_hashtag,created = Hashtag.objects.get_or_create(
                                                            tag = tag_item
                                                            )
            new_hashtag.posts.add(post)
            # print(tag_item)



parsed_hashtags.connect(parsed_hashtags_receiver)
