from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404
from django.views.generic import (
                                    ListView,
                                    DetailView,
                                    CreateView,
                                    DeleteView
                                    )
from django.urls import reverse

from .models import Hashtag

UserModel = get_user_model()#the model of users (by django)

#the list view of our hashtags
class HashtagListView(LoginRequiredMixin, ListView):
    model       = Hashtag #the hashtag model for data
    login_url   = '/login'
    #template by default hashtag_list.html
    def get_queryset(self, *args, **kwargs):#override a method
        queryset    = Hashtag.objects.all() #all hashtags
        return queryset #data



#the detail view of one hashtag
class HashtagDetailView(LoginRequiredMixin, DetailView):
    template_name   = 'hashtag/hashtag_detail.html'#the template name of the view
    login_url       = '/login'
    queryset        = Hashtag.objects.all()

    def get_object(self):
        tag_ = self.kwargs.get("tag") # get id from url
        return get_object_or_404(Hashtag,tag = tag_) # return the user or 404

    def get_context_data(self, *args, **kwargs):
        tag                         = self.kwargs.get('tag')
        context                     = super(HashtagDetailView, self).get_context_data(*args, **kwargs)
        context['fetch_post_url']   = reverse("api-post:tag", kwargs={'tag':tag})
        return context
