
from django.urls import path

from .views import (
                HashtagListView,
                HashtagDetailView
                )


app_name    = 'hashtag'

urlpatterns = [
    path('list/', HashtagListView.as_view(), name='list'),#/hashtag/list
    path('<str:tag>/', HashtagDetailView.as_view(), name='detail'),#/hashtag/create
]
